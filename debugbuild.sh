#!/bin/sh

bash ./premake.sh || exit $?
cmake -DCMAKE_BUILD_TYPE=Debug .
make -j "$(nproc)"
