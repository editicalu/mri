#!/bin/sh

bash ./premake.sh || exit $?
cmake -DCMAKE_BUILD_TYPE=Release .
make -j "$(nproc)"
