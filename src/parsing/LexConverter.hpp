#ifndef LEXCONVERTER_HPP
#define LEXCONVERTER_HPP

#include "../tokenize/TokenType.h"
#include "yaccinclude.h"

/// The LexConverter acts as the translation layer between the enums used by the tokenizer and the
/// enum used by the parser. Because these are not the same, active translation is required.
yytokentype lex_to_yacc(TokenType lex_value);

#endif
