#ifndef PARSER_HPP
#define PARSER_HPP

#include "../tokenize/TokenString.hpp"
#include "../tree/Program.hpp"

void mri_yacc_error(char *err);

/// Tries to parse the program. Returns a unique pointer to the program if it succeeded. Returns a
/// unique pointer to an error if something went wrong.
std::variant<std::unique_ptr<Program>, std::unique_ptr<Error>> parse(TokenString const &ts);

#endif
