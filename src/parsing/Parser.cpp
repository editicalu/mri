#include "Parser.hpp"
#include <string>

#include "../error/SyntaxError.hpp"
#include "../error/UnreachableCodeError.hpp"
#include "../tree/Program.hpp"
#include "LexConverter.hpp"

Token const *tokens;
size_t *position = nullptr;
size_t *token_count = nullptr;
CmpStatement *the_program;
std::optional<std::unique_ptr<Error>> error;

void mri_yacc_error(char *err) {
    std::string error_message(err);
    if (error_message == "syntax error") {
        error = std::optional{std::make_unique<SyntaxError>(err, tokens + *position)};
    } else {
        error = std::optional{std::make_unique<UnreachableCodeError>("parseerr")};
    }
}

int mri_yacc_lex() {
    if (*position < *token_count) {
        Token const &token = tokens[*position];
        *position += 1;
        char const *a = token.get_raw().data();
        mri_yacc_lval.raw = a;

        return lex_to_yacc(token.get_token_type());
    } else {
        return 0;
    }
}

std::variant<std::unique_ptr<Program>, std::unique_ptr<Error>> parse(TokenString const &ts) {
    tokens = ts.tokens.data();
    size_t this_position = 0;
    position = &this_position;
    size_t this_token_count = ts.tokens.size();
    token_count = &this_token_count;

    int result = mri_yacc_parse();
    if (result != 0) {
        // Error happened.
        return std::move(error.value());
    }
    return std::make_unique<Program>(the_program);
}
