%{
#include <memory>

#include "Parser.hpp"
#include "../tree/stm/CmpStatement.hpp"

#include "../data/function/SignatureConstructor.hpp"

#include "../tree/exp/Expression.hpp"
#include "../tree/exp/AssignmentExpression.hpp"

#include "../tree/exp/boolean/AndExpression.hpp"
#include "../tree/exp/boolean/EqExpression.hpp"
#include "../tree/exp/boolean/GeExpression.hpp"
#include "../tree/exp/boolean/GtExpression.hpp"
#include "../tree/exp/boolean/OrExpression.hpp"
#include "../tree/exp/boolean/LtExpression.hpp"
#include "../tree/exp/boolean/LeExpression.hpp"
#include "../tree/exp/boolean/NEqExpression.hpp"
#include "../tree/exp/boolean/NotExpression.hpp"
#include "../tree/exp/boolean/OrExpression.hpp"

#include "../tree/exp/function/ArgListConstructor.hpp"
#include "../tree/exp/function/FunctionCallExpression.hpp"

#include "../tree/exp/math/DivExpression.hpp"
#include "../tree/exp/math/MinExpression.hpp"
#include "../tree/exp/math/MulExpression.hpp"
#include "../tree/exp/math/SumExpression.hpp"

#include "../tree/exp/value/IntegerExpression.hpp"
#include "../tree/exp/value/FloatExpression.hpp"
#include "../tree/exp/value/BooleanExpression.hpp"
#include "../tree/exp/value/VariableExpression.hpp"

#include "../tree/stm/Statement.hpp"
#include "../tree/stm/ExpressionStatement.hpp"

#include "../tree/stm/casewhen/Case.hpp"
#include "../tree/stm/casewhen/CaseConstructor.hpp"
#include "../tree/stm/casewhen/WhenBlock.hpp"

#include "../tree/stm/data/AssignStatement.hpp"
#include "../tree/stm/data/UnDefineStatement.hpp"

#include "../tree/stm/function/FunctionDefinition.hpp"
#include "../tree/stm/function/ReturnStatement.hpp"

#include "../tree/stm/ifelse/IfElseStatement.hpp"
#include "../tree/stm/ifelse/ElseStatement.hpp"
#include "../tree/stm/ifelse/ElsIfStatement.hpp"

#include "../tree/stm/loop/UntilLoop.hpp"
#include "../tree/stm/loop/WhileLoop.hpp"


extern int mri_yacc_lex();
extern CmpStatement* the_program;
%}

%token BINOP_PLUS
%token BINOP_MINUS
%token BINOP_TIMES
%token BINOP_DIV
%token BINOP_ISEQ
%token BINOP_ISNOTEQ
%token BINOP_GT BINOP_GE BINOP_LT BINOP_LE
%token BINOP_AND BINOP_OR

%token ASSIGN_EQ
%token ASSIGN_PLUSEQ ASSIGN_MINUSEQ ASSIGN_TIMESEQ ASSIGN_DIVEQ
%token ASSIGN_ANDEQ ASSIGN_OREQ

%token TERM_NEWLINE TERM_SEMICOLON

%token SYM_IDENTIFIER SYM_INTEGER SYM_FLOAT

%token KEYWORD_UNDEF KEYWORD_DEF KEYWORD_END
%token KEYWORD_IF KEYWORD_THEN KEYWORD_ELSIF KEYWORD_ELSE KEYWORD_UNLESS
%token KEYWORD_WHILE KEYWORD_UNTIL KEYWORD_DO
%token KEYWORD_CASE KEYWORD_WHEN
%token KEYWORD_TRUE KEYWORD_FALSE
%token KEYWORD_RETURN

%token CHAR_COMMA CHAR_NOT
%token PAIRS_LPAR PAIRS_RPAR PAIRS_LBRA PAIRS_RBRA

%token NOP_COMMENT NOP_WHITESPACE

%start program

%union {
    /// Composite Statements
    CmpStatement* cmp;

    /// Statements
    Statement* stmt;
    /// For parsing else and elsif
    ElseStatement* else_stm;

    /// When-cases for the case-when
    CaseConstructor* when;

    SignatureConstructor* signature;
    ArgListConstructor* arglist;

    /// Expressions
    Expression* exp;

    /// Raw is used for endin final symbols (like integers, variable names etc). The constructor of the Expression
    /// needs to take care of the parsing.
    char const* raw;
}

%type <exp> expression
%type <stmt> statement
%type <raw> SYM_IDENTIFIER
%type <raw> SYM_INTEGER
%type <raw> SYM_FLOAT
%type <exp> assignment
%type <stmt> if_statement
%type <stmt> unless_statement
%type <stmt> case_when
%type <stmt> define_statement
%type <cmp> block
%type <cmp> compstmt

%left SYM_IDENTIFIER SYM_INTEGER SYM_FLOAT KEYWORD_TRUE KEYWORD_FALSE
%left PAIRS_LPAR PAIRS_RPAR
%left ASSIGN_EQ ASSIGN_PLUSEQ ASSIGN_MINUSEQ ASSIGN_TIMESEQ ASSIGN_DIVEQ
%left ASSIGN_ANDEQ ASSIGN_OREQ
%left BINOP_AND BINOP_OR
%left BINOP_GE BINOP_GT BINOP_LE BINOP_LT BINOP_ISEQ BINOP_ISNOTEQ
%left BINOP_PLUS BINOP_MINUS
%left BINOP_TIMES BINOP_DIV

%right UMINUS
%right CHAR_NOT
%%

program: compstmt				 { the_program = $<cmp>1; }
       ;

separators: separator separators
          | separator
          ;

separator: TERM_NEWLINE
         | TERM_SEMICOLON
         ;

compstmt: separators block      { $<cmp>$ = $<cmp>2; }
        | block                 { $<cmp>$ = $<cmp>1; }
        ;

block: statement separators block          { $<cmp>$ = new CmpStatement($<stmt>1, $<cmp>3); }
     | statement separators                { $<cmp>$ = new CmpStatement($<stmt>1, nullptr); }
     | statement                           { $<cmp>$ = new CmpStatement($<stmt>1, nullptr); }
     ;

expression: SYM_INTEGER		                               { $<exp>$ = new IntegerExpression($<raw>1); }
          | SYM_FLOAT                                          { $<exp>$ = new FloatExpression($<raw>1); }
          | KEYWORD_TRUE                                       { $<exp>$ = new BooleanExpression(true); }
          | KEYWORD_FALSE                                      { $<exp>$ = new BooleanExpression(false); }
          | SYM_IDENTIFIER                                     { $<exp>$ = new VariableExpression($<raw>1); }
	  | BINOP_MINUS expression %prec UMINUS	               { $<exp>$ = new MulExpression(new IntegerExpression(-1), $<exp>2); }
          | expression BINOP_PLUS expression                   { $<exp>$ = new SumExpression($<exp>1, $<exp>3); }
          | expression BINOP_MINUS expression                  { $<exp>$ = new MinExpression($<exp>1, $<exp>3); }
          | expression BINOP_TIMES expression                  { $<exp>$ = new MulExpression($<exp>1, $<exp>3); }
          | expression BINOP_DIV expression                    { $<exp>$ = new DivExpression($<exp>1, $<exp>3); }
          | expression BINOP_AND expression	               { $<exp>$ = new AndExpression($<exp>1, $<exp>3); }
          | expression BINOP_ISEQ expression	               { $<exp>$ = new EqExpression($<exp>1, $<exp>3); }
          | expression BINOP_GE expression	               { $<exp>$ = new GeExpression($<exp>1, $<exp>3); }
          | expression BINOP_GT expression	               { $<exp>$ = new GtExpression($<exp>1, $<exp>3); }
          | expression BINOP_LE expression	               { $<exp>$ = new LeExpression($<exp>1, $<exp>3); }
          | expression BINOP_LT expression	               { $<exp>$ = new LtExpression($<exp>1, $<exp>3); }
          | expression BINOP_ISNOTEQ expression	               { $<exp>$ = new NEqExpression($<exp>1, $<exp>3); }
          | CHAR_NOT expression	       	                       { $<exp>$ = new NotExpression($<exp>2); }
          | expression BINOP_OR expression	               { $<exp>$ = new OrExpression($<exp>1, $<exp>3); }
          | PAIRS_LPAR expression PAIRS_RPAR                   { $<exp>$ = $<exp>2; }
          | assignment                                         { $<exp>$ = $<exp>1; }
          | SYM_IDENTIFIER PAIRS_LPAR expressions PAIRS_RPAR   { $<exp>$ = new FunctionCallExpression($<raw>1, $<arglist>3); }
          | SYM_IDENTIFIER PAIRS_LPAR PAIRS_RPAR               { $<exp>$ = new FunctionCallExpression($<raw>1, nullptr); }
          ;

expressions: expression                                   { $<arglist>$ = new ArgListConstructor($<exp>1, nullptr); }
           | expression CHAR_COMMA expressions            { $<arglist>$ = new ArgListConstructor($<exp>1, $<arglist>3); }
           ;

assignment: SYM_IDENTIFIER ASSIGN_EQ expression	       { $<exp>$ = new AssignmentExpression($<raw>1, $<exp>3); }
          | SYM_IDENTIFIER ASSIGN_PLUSEQ expression    { $<exp>$ = new AssignmentExpression($<raw>1, new SumExpression(new VariableExpression($<raw>1), $<exp>3)); }
          | SYM_IDENTIFIER ASSIGN_MINUSEQ expression   { $<exp>$ = new AssignmentExpression($<raw>1, new MinExpression(new VariableExpression($<raw>1), $<exp>3)); }
          | SYM_IDENTIFIER ASSIGN_TIMESEQ expression   { $<exp>$ = new AssignmentExpression($<raw>1, new MulExpression(new VariableExpression($<raw>1), $<exp>3)); }
          | SYM_IDENTIFIER ASSIGN_DIVEQ expression     { $<exp>$ = new AssignmentExpression($<raw>1, new DivExpression(new VariableExpression($<raw>1), $<exp>3)); }
          | SYM_IDENTIFIER ASSIGN_ANDEQ expression     { $<exp>$ = new AssignmentExpression($<raw>1, new AndExpression(new VariableExpression($<raw>1), $<exp>3)); }
          | SYM_IDENTIFIER ASSIGN_OREQ expression      { $<exp>$ = new AssignmentExpression($<raw>1, new OrExpression(new VariableExpression($<raw>1), $<exp>3)); }
          ;

if_statement: KEYWORD_IF expression then_separator compstmt elseif	{ $<stmt>$ = new IfElseStatement($<exp>2, $<cmp>4, $<else_stm>5); }
            ;
unless_statement: KEYWORD_UNLESS expression then_separator compstmt KEYWORD_END                        { $<stmt>$ = new IfElseStatement(new NotExpression($<exp>2), $<cmp>4, nullptr); }
                | KEYWORD_UNLESS expression then_separator compstmt KEYWORD_ELSE compstmt KEYWORD_END  { $<stmt>$ = new IfElseStatement(new NotExpression($<exp>2), $<cmp>4, new ElseStatement($<cmp>6)); }

elseif: KEYWORD_ELSIF expression then_separator compstmt elseif         { $<else_stm>$ = new ElsIfStatement($<exp>2, $<cmp>4, $<else_stm>5); }
      | KEYWORD_ELSE compstmt KEYWORD_END		                { $<else_stm>$ = new ElseStatement($<cmp>2); }
      | KEYWORD_END						        { $<else_stm>$ = nullptr; }
      ;

then_separator: separator
      	      | KEYWORD_THEN
              | separator KEYWORD_THEN
      	      ;

do_separator: KEYWORD_DO
      | separator
      | separator KEYWORD_DO
      ;

case_when: KEYWORD_CASE expression when_blocks KEYWORD_END                              { $<stmt>$ = new Case($<exp>2, $<when>3); }
	 | KEYWORD_CASE expression when_blocks KEYWORD_ELSE compstmt KEYWORD_END        { $<stmt>$ = new Case($<exp>2, $<when>3, $<cmp>5); }
	 ;

when_blocks: KEYWORD_WHEN expression then_separator compstmt               { $<when>$ = new CaseConstructor(new WhenBlock($<exp>2, $<cmp>4), nullptr); }
           | KEYWORD_WHEN expression then_separator compstmt when_blocks   { $<when>$ = new CaseConstructor(new WhenBlock($<exp>2, $<cmp>4), $<when>5); }
	   ;

signature: SYM_IDENTIFIER CHAR_COMMA signature   { $<signature>$ = new SignatureConstructor($<raw>1, $<signature>3); }
         | SYM_IDENTIFIER                        { $<signature>$ = new SignatureConstructor($<raw>1, nullptr); }
         ;

define_statement: KEYWORD_DEF SYM_IDENTIFIER PAIRS_LPAR signature PAIRS_RPAR compstmt KEYWORD_END { $<stmt>$ = new FunctionDefinition($<raw>2, new UserFunction($<signature>4, $<cmp>6)); }
                | KEYWORD_DEF SYM_IDENTIFIER PAIRS_LPAR PAIRS_RPAR compstmt KEYWORD_END           { $<stmt>$ = new FunctionDefinition($<raw>2, new UserFunction(nullptr, $<cmp>5)); }
                ;

statement: if_statement
         | unless_statement
	 | KEYWORD_UNDEF SYM_IDENTIFIER			                      { $<stmt>$ = new UnDefineStatement($<raw>2); }
	 | KEYWORD_UNTIL expression do_separator compstmt KEYWORD_END        { $<stmt>$ = new UntilLoop($<exp>2, $<cmp>4); }
	 | KEYWORD_WHILE expression do_separator compstmt KEYWORD_END         { $<stmt>$ = new WhileLoop($<exp>2, $<cmp>4); }
	 | case_when                                                          { $<stmt>$ = $<stmt>1; }
	 | define_statement                                                   { $<stmt>$ = $<stmt>1; }
	 | KEYWORD_RETURN expression                                          { $<stmt>$ = new ReturnStatement($<exp>2); }
	 | expression				                              { $<stmt>$ = new ExpressionStatement($<exp>1); }
	 ;
%%
int yywarp() {
    return 1;
}
