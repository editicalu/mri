#ifndef YACCINCLUDE_H
#define YACCINCLUDE_H

// Include this header when you need ruby.tab.h
#include "../data/function/SignatureConstructor.hpp"
#include "../tree/exp/Expression.hpp"
#include "../tree/exp/function/ArgListConstructor.hpp"
#include "../tree/stm/CmpStatement.hpp"
#include "../tree/stm/Statement.hpp"
#include "../tree/stm/casewhen/CaseConstructor.hpp"
#include "../tree/stm/ifelse/ElseStatement.hpp"
#include "Parser.hpp"

#include "ruby.tab.h"

#endif
