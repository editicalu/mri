#include "LexConverter.hpp"

yytokentype lex_to_yacc(TokenType lex_value) {
    switch (lex_value) {
    case LEX_BINOP_PLUS:
        return BINOP_PLUS;
    case LEX_BINOP_MINUS:
        return BINOP_MINUS;
    case LEX_BINOP_TIMES:
        return BINOP_TIMES;
    case LEX_BINOP_DIV:
        return BINOP_DIV;
    case LEX_BINOP_ISEQ:
        return BINOP_ISEQ;
    case LEX_BINOP_ISNOTEQ:
        return BINOP_ISNOTEQ;
    case LEX_BINOP_GT:
        return BINOP_GT;
    case LEX_BINOP_GE:
        return BINOP_GE;
    case LEX_BINOP_LT:
        return BINOP_LT;
    case LEX_BINOP_LE:
        return BINOP_LE;
    case LEX_BINOP_AND:
        return BINOP_AND;
    case LEX_BINOP_OR:
        return BINOP_OR;
    case LEX_ASSIGN_EQ:
        return ASSIGN_EQ;
    case LEX_ASSIGN_PLUSEQ:
        return ASSIGN_PLUSEQ;
    case LEX_ASSIGN_MINUSEQ:
        return ASSIGN_MINUSEQ;
    case LEX_ASSIGN_TIMESEQ:
        return ASSIGN_TIMESEQ;
    case LEX_ASSIGN_DIVEQ:
        return ASSIGN_DIVEQ;
    case LEX_ASSIGN_ANDEQ:
        return ASSIGN_ANDEQ;
    case LEX_ASSIGN_OREQ:
        return ASSIGN_OREQ;
    case LEX_TERM_NEWLINE:
        return TERM_NEWLINE;
    case LEX_TERM_SEMICOLON:
        return TERM_SEMICOLON;
    case LEX_SYM_IDENTIFIER:
        return SYM_IDENTIFIER;
    case LEX_SYM_INTEGER:
        return SYM_INTEGER;
    case LEX_SYM_FLOAT:
        return SYM_FLOAT;
    case LEX_KEYWORD_UNDEF:
        return KEYWORD_UNDEF;
    case LEX_KEYWORD_DEF:
        return KEYWORD_DEF;
    case LEX_KEYWORD_END:
        return KEYWORD_END;
    case LEX_KEYWORD_THEN:
        return KEYWORD_THEN;
    case LEX_KEYWORD_IF:
        return KEYWORD_IF;
    case LEX_KEYWORD_ELSIF:
        return KEYWORD_ELSIF;
    case LEX_KEYWORD_ELSE:
        return KEYWORD_ELSE;
    case LEX_KEYWORD_UNLESS:
        return KEYWORD_UNLESS;
    case LEX_KEYWORD_WHILE:
        return KEYWORD_WHILE;
    case LEX_KEYWORD_CASE:
        return KEYWORD_CASE;
    case LEX_KEYWORD_WHEN:
        return KEYWORD_WHEN;
    case LEX_KEYWORD_DO:
        return KEYWORD_DO;
    case LEX_KEYWORD_TRUE:
        return KEYWORD_TRUE;
    case LEX_KEYWORD_FALSE:
        return KEYWORD_FALSE;
    case LEX_KEYWORD_RETURN:
        return KEYWORD_RETURN;
    case LEX_KEYWORD_UNTIL:
        return KEYWORD_UNTIL;
    case LEX_CHAR_COMMA:
        return CHAR_COMMA;
    case LEX_CHAR_NOT:
        return CHAR_NOT;
    case LEX_PAIRS_LPAR:
        return PAIRS_LPAR;
    case LEX_PAIRS_RPAR:
        return PAIRS_RPAR;
    case LEX_PAIRS_LBRA:
        return PAIRS_LBRA;
    case LEX_PAIRS_RBRA:
        return PAIRS_RBRA;
    case LEX_NOP_WHITESPACE:
        return NOP_WHITESPACE;
    case LEX_NOP_COMMENT:
        return NOP_COMMENT;
    }
    return NOP_COMMENT;
}