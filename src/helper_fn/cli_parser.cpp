#include "cli_parser.hpp"

std::pair<bool, std::optional<std::string>> read_source(int argc, char **argv) {
    std::optional<std::string> input_file = std::nullopt;
    if (argc < 2) {
        // No parameters were given. We should read from the standard input.
        return std::pair(true, std::nullopt);
    } else {
        std::string s(argv[1]);
        if (s == "-h" || s == "--help") {
            return std::pair(false, std::nullopt);
        } else {
            return std::pair(true, s);
        }
    }
}
