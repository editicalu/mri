#pragma once
#ifndef READER_HPP
#define READER_HPP

#include <optional>
#include <string>

/// Reads where the input should be coming from.
///
/// The first value in the pair is whether the pair contains a valid reader source (i.e. false if
/// the user asked for help).
///
/// The second value returns an optional. This has a value if the user provided an input file and
/// the value is valid (i.e. if the first in the pair is true).
std::pair<bool, std::optional<std::string>> read_source(int argc, char **argv);

#endif