#include <any>
#include <iostream>
#include <optional>
#include <string>
#include <variant>
#include <vector>

#include "data/function/stdlib/StandardLibrary.hpp"
#include "helper_fn/cli_parser.hpp"
#include "parsing/Parser.hpp"
#include "tokenize/TokenString.hpp"
#include "tree/Program.hpp"

auto read_tokens(std::pair<bool, std::optional<std::string>> const &reader_source) {
    if (reader_source.second.has_value()) {
#ifdef DEBUG
        std::cerr << "Reading from file: " << reader_source.second.value() << std::endl;
#endif
        return TokenString::try_tokenize_file(reader_source.second.value());
    }
#ifdef DEBUG
    std::cerr << "Reading from stdin" << std::endl;
#endif
    auto a = TokenString::try_tokenize_stdin();
    return a;
}

auto main(int argc, char **argv) -> int {
    std::pair<bool, std::optional<std::string>> reader_source = read_source(argc, argv);
    if (!reader_source.first) {
        std::cout << "USAGE" << std::endl
                  << "To read from a file:" << std::endl
                  << "\t" << argv[0] << " PATH_TO_SOURCE_FILE" << std::endl
                  << "To read from the stdin:" << std::endl
                  << "\t" << argv[0] << std::endl
                  << std::endl;

        return EXIT_FAILURE;
    }

    auto ts_opt = read_tokens(reader_source);
    switch (ts_opt.index()) {
    case 0: {
        TokenString ts = std::get<TokenString>(ts_opt);
        // Remove white space etc. from the tokenstring
        ts.clean();
#ifdef DEBUG
        ts.print_debug();
#endif

        // Build the actual syntax tree
        auto p_result = parse(ts);
        if (p_result.index() == 1) {
            std::unique_ptr<Error> const &p_error = std::get<std::unique_ptr<Error>>(p_result);
            p_error->explain_on_stderr();
            return EXIT_FAILURE;
        }

        std::unique_ptr<Program> p = std::move(std::get<std::unique_ptr<Program>>(p_result));

        StandardLibrary sl;
        SymbolTable system_table(nullptr);
        sl.add_to_symbol_table(system_table);
        SymbolTable user_table(&system_table);

        // Print the eventual SymbolTable if
        auto result = p->go(user_table);
        if (result.index() == 0) {
            // Not a single error happened
            auto return_value = std::get<ReturnValue>(result);
#ifdef DEBUG
            user_table.print();
            std::cerr << std::endl;

            if (return_value.has_value()) {
                std::cerr << "Program returned ";
                return_value.value().print();
            } else {
                std::cerr << "Program did not return a value" << std::endl;
            }
#endif
            return EXIT_SUCCESS;
        } else {
            // RuntimeError
            std::unique_ptr<Error> error = std::move(std::get<std::unique_ptr<Error>>(result));
            error->explain_on_stderr();
            return EXIT_FAILURE;
        }
    }
    case 1: {
        std::unique_ptr<Error> te = std::get<std::unique_ptr<Error>>(std::move(ts_opt));
        te->explain_on_stderr();
        return EXIT_FAILURE;
    }
    default: {
        return EXIT_FAILURE;
    }
    }
}
