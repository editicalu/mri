#ifndef PROGRAM_HPP
#define PROGRAM_HPP

#include <memory>
#include <vector>

#include "stm/CmpStatement.hpp"
#include "stm/Statement.hpp"

/// Program is the class that holds the program as is.
class Program {
    std::vector<std::unique_ptr<Statement>> program;

  public:
    Program(CmpStatement *statements);

    /// Run this program. Returns the final SymbolTable and return value (if available).
    [[nodiscard]] std::variant<ReturnValue, std::unique_ptr<Error>> go(SymbolTable &st) const;
};

#endif
