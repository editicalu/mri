#ifndef EXPRESSIONSTATEMENT_HPP
#define EXPRESSIONSTATEMENT_HPP

#include "../exp/Expression.hpp"
#include "Statement.hpp"

/// Expressions can be run without any statement to be put in. Examples of this include the generic
class ExpressionStatement : public Statement {
    std::unique_ptr<Expression> exp;

  public:
    explicit ExpressionStatement(Expression *e);

    virtual StatementResult execute(SymbolTable &st, bool try_evaluate) const;
};

#endif
