#include "ExpressionStatement.hpp"

ExpressionStatement::ExpressionStatement(Expression *exp) : exp(exp) {}

StatementResult ExpressionStatement::execute(SymbolTable &st, bool try_evaluate) const {
    ExpressionResult er = this->exp->express(st);
    if (er.index() == 1) {
        // Error occurred
        return std::get<std::unique_ptr<Error>>(std::move(er));
    } else {

        return try_evaluate ? std::optional{std::get<Value>(er)} : std::nullopt;
    }
}
