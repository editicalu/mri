#ifndef CASECONSTRUCTOR_HPP
#define CASECONSTRUCTOR_HPP

#include "WhenBlock.hpp"
#include <memory>
#include <optional>

struct CaseConstructor {
    std::unique_ptr<WhenBlock> when_block;
    std::optional<std::unique_ptr<CaseConstructor>> next;

  public:
    explicit CaseConstructor(WhenBlock *wb, CaseConstructor *next = nullptr);

    std::vector<std::unique_ptr<WhenBlock>> into_list();
};

#endif
