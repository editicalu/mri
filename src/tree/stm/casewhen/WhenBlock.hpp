#ifndef WHENBLOCK_HPP
#define WHENBLOCK_HPP

#include "../../exp/Expression.hpp"
#include "../BlockStatement.hpp"

class WhenBlock {
  public:
    std::unique_ptr<Expression> e;
    std::unique_ptr<BlockStatement> bs;
    WhenBlock(Expression *e, CmpStatement *cs);
};

#endif
