#include "WhenBlock.hpp"

WhenBlock::WhenBlock(Expression *e, CmpStatement *cs)
    : e(e), bs(std::make_unique<BlockStatement>(cs)) {}
