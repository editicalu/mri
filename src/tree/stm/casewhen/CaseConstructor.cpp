#include "CaseConstructor.hpp"

CaseConstructor::CaseConstructor(WhenBlock *wb, CaseConstructor *next)
    : when_block(wb), next(next ? std::optional{next} : std::nullopt) {}

std::vector<std::unique_ptr<WhenBlock>> CaseConstructor::into_list() {
    std::vector<std::unique_ptr<WhenBlock>> list;
    list.push_back(std::move(this->when_block));
    std::optional<std::unique_ptr<CaseConstructor>> cc = std::move(this->next);
    while (cc.has_value()) {
        list.push_back(std::move(cc.value()->when_block));
        cc = std::move(cc.value()->next);
    }
    return list;
}
