#include "Case.hpp"

#include "../../../error/TypeError.hpp"
#include "../../../error/UnreachableCodeError.hpp"

Case::Case(Expression *e, CaseConstructor *cc, CmpStatement *else_block)
    : exp(e),
      when_blocks(cc->into_list()),
      else_block(
          else_block == nullptr ? std::nullopt
                                : std::optional{std::make_unique<BlockStatement>(else_block)}) {
    delete cc;
}

StatementResult Case::execute(SymbolTable &st, bool try_evaluate) const {
    ExpressionResult e = this->exp->express(st);
    if (e.index() == 1) {
        return std::move(std::get<std::unique_ptr<Error>>(e));
    }
    Value v = std::get<Value>(std::move(e));
    DataType dt = v.get_type();

    for (auto const &when_block : this->when_blocks) {
        ExpressionResult e2 = when_block->e->express(st);
        if (e2.index() == 1) {
            return std::move(std::get<std::unique_ptr<Error>>(e2));
        }
        Value v2 = std::get<Value>(std::move(e2));

        if (dt != v2.get_type()) {
            return std::make_unique<TypeError>("==", dt, v2.get_type());
        }

        if (dt == TYPE_INT) {
            if (v.get_int() == v2.get_int()) {
                return when_block->bs->execute(st, try_evaluate);
            }
        } else if (dt == TYPE_FLOAT) {
            if (v.get_float() == v2.get_float()) {
                return when_block->bs->execute(st, try_evaluate);
            }
        } else if (dt == TYPE_BOOL) {
            if (v.get_bool() == v2.get_bool()) {
                return when_block->bs->execute(st, try_evaluate);
            }
        } else if (dt == TYPE_STRING) {
            if (v.get_string() == v2.get_string()) {
                return when_block->bs->execute(st, try_evaluate);
            }
        } else {
            return std::make_unique<UnreachableCodeError>("case");
        }
    }
    // Nothing found. Run else
    if (this->else_block.has_value()) {
        return this->else_block.value()->execute(st, try_evaluate);
    } else {
        return std::nullopt;
    }
}
