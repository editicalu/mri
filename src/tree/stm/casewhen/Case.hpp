#ifndef CASE_HPP
#define CASE_HPP

#include "../BlockStatement.hpp"
#include "../Statement.hpp"
#include "CaseConstructor.hpp"
#include "WhenBlock.hpp"

class Case : public Statement {
    std::unique_ptr<Expression> exp;
    std::vector<std::unique_ptr<WhenBlock>> when_blocks;
    std::optional<std::unique_ptr<BlockStatement>> else_block;

  public:
    explicit Case(Expression *e, CaseConstructor *cc, CmpStatement *else_block = nullptr);
    virtual StatementResult execute(SymbolTable &st, bool try_evaluate) const;
};

#endif
