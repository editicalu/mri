#include "AssignStatement.hpp"

AssignStatement::AssignStatement(char const *identifier, Expression *value)
    : identifier(identifier), value(std::unique_ptr<Expression>(value)) {}

StatementResult AssignStatement::execute(SymbolTable &st, bool try_evaluate) const {
    ExpressionResult a = this->value->express(st);
    if (a.index() == 1) {
        return std::move(std::get<std::unique_ptr<Error>>(a));
    }
    Value a_value = std::get<Value>(a);
    st.update_or_insert(this->identifier, a_value);
    if (try_evaluate) {
        return std::optional{a_value};
    } else {
        return std::nullopt;
    }
}
