#include "UnDefineStatement.hpp"
#include "../../../error/SymbolError.hpp"

UnDefineStatement::UnDefineStatement(char const *identifier) : identifier(identifier) {
    // delete identifier;
}

StatementResult UnDefineStatement::execute(SymbolTable &st, bool try_evaluate) const {
    if (st.delete_name(this->identifier)) {
        return std::nullopt;
    } else {
        // The specified variable name does not exist.
        return std::make_unique<SymbolError>(this->identifier);
    }
}
