#ifndef ASSIGNSTATEMENT_HPP
#define ASSIGNSTATEMENT_HPP

#include "../../exp/Expression.hpp"
#include "../Statement.hpp"

class AssignStatement : public Statement {

    std::string identifier;
    std::unique_ptr<Expression> value;

  public:
    AssignStatement(char const *identifier, Expression *value);

    StatementResult execute(SymbolTable &st, bool try_evaluate) const override;
};

#endif
