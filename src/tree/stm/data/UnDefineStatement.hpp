#ifndef UNDEFINESTATEMENT_HPP
#define UNDEFINESTATEMENT_HPP

#include "../Statement.hpp"

class UnDefineStatement : public Statement {
    std::string identifier;

  public:
    explicit UnDefineStatement(char const *identifier);

    StatementResult execute(SymbolTable &st, bool try_evaluate) const override;
};

#endif
