#include "CmpStatement.hpp"

CmpStatement::CmpStatement(Statement *s1, CmpStatement *the_next)
    : s1(s1), next(the_next == nullptr ? std::nullopt : std::optional{the_next}) {}

std::vector<std::unique_ptr<Statement>> CmpStatement::into_statement_list() {
    std::vector<std::unique_ptr<Statement>> list;
    if (this->s1 == nullptr) {
        return list;
    }
    list.push_back(std::move(this->s1));
    std::optional<std::unique_ptr<CmpStatement>> cmpstm = std::move(this->next);
    while (cmpstm.has_value()) {
        list.push_back(std::move(cmpstm.value()->s1));
        cmpstm = std::move(cmpstm.value()->next);
    }
    return list;
}
