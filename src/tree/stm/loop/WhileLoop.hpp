#ifndef WHILELOOP_HPP
#define WHILELOOP_HPP

#include "../../exp/Expression.hpp"
#include "../BlockStatement.hpp"
#include "../CmpStatement.hpp"
#include "../Statement.hpp"

/// WhileLoop represents a while-loop in the code. This loop requires a Expression that evaluates to
/// a boolean.
class WhileLoop : public Statement {
    /// Test that is executed before each iteration of the while-loop. This should always evaluate
    /// to a boolean.
    std::unique_ptr<Expression> test;

    /// Block that is executed for as long as the test evaluates to true.
    std::unique_ptr<BlockStatement> block;

  public:
    WhileLoop(Expression *e, CmpStatement *cs);

    StatementResult execute(SymbolTable &st, bool try_evaluate) const override;
};

#endif
