#include "UntilLoop.hpp"
#include "../../../error/TypeError.hpp"

UntilLoop::UntilLoop(Expression *e, CmpStatement *cs)
    : test(e), block(std::make_unique<BlockStatement>(cs)) {}

StatementResult UntilLoop::execute(SymbolTable &st, bool try_evaluate) const {
    while (true) {
        StatementResult sr = std::nullopt;
        ExpressionResult er = this->test->express(st);
        if (er.index() == 1) {
            return std::move(std::get<std::unique_ptr<Error>>((er)));
        } else {
            Value v = std::get<Value>(er);
            if (v.get_type() != TYPE_BOOL) {
                return std::make_unique<TypeError>("while", v.get_type());
            } else {
                if (v.get_bool()) {
                    return sr;
                } else {
                    sr = this->block->execute(st, try_evaluate);
                }
            }
        }
    }
}
