#ifndef UNLESSLOOP_HPP
#define UNLESSLOOP_HPP

#include "../../exp/Expression.hpp"
#include "../BlockStatement.hpp"
#include "../Statement.hpp"

class UntilLoop : public Statement {
    /// Test that is executed before each iteration of the loop. This should always evaluate
    /// to a boolean.
    std::unique_ptr<Expression> test;

    /// Block that is executed for as long as the test evaluates to true.
    std::unique_ptr<BlockStatement> block;

  public:
    UntilLoop(Expression *e, CmpStatement *cs);

    StatementResult execute(SymbolTable &st, bool try_evaluate) const override;
};

#endif
