#include "BlockStatement.hpp"

BlockStatement::BlockStatement(CmpStatement *cs) : statements(cs->into_statement_list()) {
    delete cs;
}

StatementResult BlockStatement::execute(SymbolTable &st, bool try_evaluate) const {
    SymbolTable new_st(&st);
    for (size_t index = 0; index < this->statements.size() - 1; ++index) {
        std::unique_ptr<Statement> const &stmt = this->statements[index];
        StatementResult statement_result = stmt->execute(new_st, false);
        if (statement_result.index() == 1) {
            // Errors are propagated upwards.
            return statement_result;
        } else {
            // Check if we returned.
            std::optional<Value> return_value = std::get<std::optional<Value>>(statement_result);
            if (return_value.has_value()) {
                return return_value;
            }
        }
    }

    // As we did not yet return, the last statement should be the return statement.
    std::unique_ptr<Statement> const &stmt = this->statements[this->statements.size() - 1];
    StatementResult statement_result = stmt->execute(new_st, try_evaluate);
    return statement_result;
}
