#include "IfElseStatement.hpp"
#include "../../../error/TypeError.hpp"

IfElseStatement::IfElseStatement(
    Expression *expression,
    CmpStatement *if_block,
    ElseStatement *else_block)
    : expression(expression),
      if_block(std::make_unique<BlockStatement>(if_block)),
      else_block(else_block ? std::optional{else_block} : std::nullopt) {}

StatementResult IfElseStatement::execute(SymbolTable &st, bool try_evaluate) const {
    Expression const &exp = *this->expression;
    ExpressionResult res = exp.express(st);
    if (res.index() == 1) {
        // Expression returned an error
        return std::move(std::get<std::unique_ptr<Error>>(res));
    } else {
        Value v = std::get<Value>(res);
        if (v.get_type() != TYPE_BOOL) {
            // Cannot compare anything other than booleans.
            return std::make_unique<TypeError>("if", v.get_type());
        } else {
            // The actual if-test
            if (v.get_bool()) {
                return this->if_block->execute(st, try_evaluate);
            } else {
                if (this->else_block.has_value()) {
                    return this->else_block.value()->execute(st, try_evaluate);
                } else {
                    // No else means no error
                    return std::nullopt;
                }
            }
        }
    }
}
