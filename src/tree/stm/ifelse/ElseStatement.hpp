#ifndef ELSESTATEMENT_HPP
#define ELSESTATEMENT_HPP

#include "../BlockStatement.hpp"
#include "../CmpStatement.hpp"
#include "../Statement.hpp"

class ElseStatement : public Statement {
  protected:
    std::unique_ptr<BlockStatement> block;

  public:
    explicit ElseStatement(CmpStatement *statements);

    StatementResult execute(SymbolTable &st, bool try_evaluate) const override;
};

#endif
