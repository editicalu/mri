#ifndef IFELSESTATEMENT_HPP
#define IFELSESTATEMENT_HPP

#include "../../exp/Expression.hpp"
#include "../CmpStatement.hpp"
#include "../Statement.hpp"
#include "ElseStatement.hpp"

class IfElseStatement : public Statement {
    std::unique_ptr<Expression> expression;
    std::unique_ptr<BlockStatement> if_block;
    std::optional<std::unique_ptr<ElseStatement>> else_block;

  public:
    IfElseStatement(Expression *expression, CmpStatement *if_block, ElseStatement *else_block);

    virtual StatementResult execute(SymbolTable &st, bool try_evaluate) const;
};

#endif
