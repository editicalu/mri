#include "ElseStatement.hpp"

ElseStatement::ElseStatement(CmpStatement *statements)
    : block(std::make_unique<BlockStatement>(statements)) {}

StatementResult ElseStatement::execute(SymbolTable &st, bool try_evaluate) const {
    return this->block->execute(st, try_evaluate);
}
