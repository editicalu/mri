#include "ElsIfStatement.hpp"
#include "../../../error/TypeError.hpp"

ElsIfStatement::ElsIfStatement(
    Expression *expression,
    CmpStatement *if_block,
    ElseStatement *else_block)
    : ElseStatement(if_block),
      expression(expression),
      else_block(else_block != nullptr ? std::optional{else_block} : std::nullopt) {}

StatementResult ElsIfStatement::execute(SymbolTable &st, bool try_evaluate) const {
    Expression const &exp = *this->expression;
    ExpressionResult res = exp.express(st);
    if (res.index() == 1) {
        // Expression returned an error
        return std::move(std::get<std::unique_ptr<Error>>(res));
    } else {
        Value v = std::get<Value>(res);
        if (v.get_type() != TYPE_BOOL) {
            // Cannot compare anything other than booleans.
            return std::make_unique<TypeError>("if", v.get_type());
        } else {
            // The actual if-test
            if (v.get_bool()) {
                return this->block->execute(st, try_evaluate);
            } else {
                if (else_block.has_value()) {
                    ElseStatement const &es = *else_block.value();
                    return es.execute(st, try_evaluate);
                } else {
                    // No else
                    return std::nullopt;
                }
            }
        }
    }
}
