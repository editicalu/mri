#ifndef ELSIFSTATEMENT_HPP
#define ELSIFSTATEMENT_HPP

#include "../../exp/Expression.hpp"
#include "ElseStatement.hpp"

class ElsIfStatement : public ElseStatement {
    std::unique_ptr<Expression> expression;
    std::optional<std::unique_ptr<ElseStatement>> else_block;

  public:
    ElsIfStatement(Expression *expression, CmpStatement *if_block, ElseStatement *else_block);
    StatementResult execute(SymbolTable &st, bool try_evaluate) const override;
};

#endif
