#ifndef CMPSTATEMENT_HPP
#define CMPSTATEMENT_HPP

#include "Statement.hpp"

/// Helper class, used in the parser to parse a tree. Will be converted into a Program. This item
/// should NEVER be put in a std::unique_ptr, as a call to this->into_statement_list() will
/// deallocate it.
class CmpStatement {
  public:
    std::unique_ptr<Statement> s1;
    std::optional<std::unique_ptr<CmpStatement>> next;

    explicit CmpStatement(Statement *s1, CmpStatement *next = nullptr);

    std::vector<std::unique_ptr<Statement>> into_statement_list();
};

#endif
