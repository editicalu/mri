#ifndef RETURNSTATEMENT_HPP
#define RETURNSTATEMENT_HPP

#include "../../exp/Expression.hpp"
#include "../Statement.hpp"

class ReturnStatement : public Statement {
    std::unique_ptr<Expression> expression;

  public:
    explicit ReturnStatement(Expression *expression);

    StatementResult execute(SymbolTable &st, bool try_evaluate) const override;
};

#endif
