#ifndef FUNCTIONDEFINITION_HPP
#define FUNCTIONDEFINITION_HPP

#include "../../../data/function/UserFunction.hpp"
#include "../CmpStatement.hpp"
#include "../Statement.hpp"

/// Represents the 'def' statement which declares and defines a function. The function can be called
/// as soon as this statement is called, including in the defining function. Hence, recursive calls
/// are possible.
class FunctionDefinition : public Statement {
    std::string name;
    std::unique_ptr<UserFunction> function;

  public:
    FunctionDefinition(char const *name, UserFunction *function);

    StatementResult execute(SymbolTable &st, bool try_evaluate) const override;
};

#endif
