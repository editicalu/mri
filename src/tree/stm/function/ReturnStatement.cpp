//
// Created by wardsegers on 30/12/2019.
//

#include "ReturnStatement.hpp"

ReturnStatement::ReturnStatement(Expression *expression) : expression(expression) {}

StatementResult ReturnStatement::execute(SymbolTable &st, bool try_evaluate) const {
    ExpressionResult er = this->expression->express(st);
    if (er.index() == 1) {
        return std::get<std::unique_ptr<Error>>(std::move(er));
    } else {
        return std::get<Value>(er);
    }
}
