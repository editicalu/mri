#include "FunctionDefinition.hpp"

FunctionDefinition::FunctionDefinition(char const *name, UserFunction *function)
    : name(name), function(function) {
    // delete name;
}

StatementResult FunctionDefinition::execute(SymbolTable &st, bool try_evaluate) const {
    st.update_or_insert(this->name, Value(this->function.get()));
    // This statement does not return
    return std::nullopt;
}
