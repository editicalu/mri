#ifndef STATEMENT_HPP
#define STATEMENT_HPP

#include "../../data/SymbolTable.hpp"
#include "../../error/Error.hpp"
#include <optional>

typedef std::optional<Value> ReturnValue;
typedef std::variant<std::optional<Value>, std::unique_ptr<Error>> StatementResult;

class Statement {
  public:
    /// Execute this statement, given the SymbolTable.
    /// If try_evaluate is true, the statement will test if it can be evaluated into a Value. This
    /// should be used to implicitly create a return statement at the end of a block.
    virtual StatementResult execute(SymbolTable &st, bool try_evaluate) const = 0;
};

#endif
