#ifndef BLOCKSTATEMENT_HPP
#define BLOCKSTATEMENT_HPP

#include "CmpStatement.hpp"
#include "Statement.hpp"

class BlockStatement : public Statement {
    std::vector<std::unique_ptr<Statement>> statements;

  public:
    explicit BlockStatement(CmpStatement *cs);

    StatementResult execute(SymbolTable &st, bool try_evaluate) const override;
};

#endif
