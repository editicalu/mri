#include "Program.hpp"

Program::Program(CmpStatement *statements) : program(statements->into_statement_list()) {}

std::variant<ReturnValue, std::unique_ptr<Error>> Program::go(SymbolTable &st) const {
    for (size_t idx = 0; idx < this->program.size() - 1; ++idx) {
        auto const &stmt = this->program[idx];
        StatementResult statement_result = stmt->execute(st, false);
        if (statement_result.index() == 1) {
            // Errors are propagated upwards.
            return std::get<std::unique_ptr<Error>>(std::move(statement_result));
        } else {
            // Check if we returned.
            std::optional<Value> return_value = std::get<std::optional<Value>>(statement_result);
            if (return_value.has_value()) {
                return std::move(return_value);
            }
        }
    }

    // The last statement should return.
    size_t index = this->program.size() - 1;
    return this->program[index]->execute(st, true);

    // No errors and no return value
    return std::nullopt;
}