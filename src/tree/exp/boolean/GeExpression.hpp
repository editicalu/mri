#ifndef GEEXPRESSION_HPP
#define GEEXPRESSION_HPP

#include "../Expression.hpp"

class GeExpression : public Expression {
    std::unique_ptr<Expression> e1;
    std::unique_ptr<Expression> e2;

  public:
    GeExpression(Expression *e1, Expression *e2);

    ExpressionResult express(SymbolTable &st) const override;
};

#endif
