#ifndef GTEXPRESSION_HPP
#define GTEXPRESSION_HPP

#include "../Expression.hpp"

class GtExpression : public Expression {
    std::unique_ptr<Expression> e1;
    std::unique_ptr<Expression> e2;

  public:
    GtExpression(Expression *e1, Expression *e2);

    ExpressionResult express(SymbolTable &st) const override;
};

#endif
