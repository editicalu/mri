#ifndef LTEXPRESSION_HPP
#define LTEXPRESSION_HPP

#include "../Expression.hpp"

class LtExpression : public Expression {
    std::unique_ptr<Expression> e1;
    std::unique_ptr<Expression> e2;

  public:
    LtExpression(Expression *e1, Expression *e2);

    ExpressionResult express(SymbolTable &st) const override;
};

#endif
