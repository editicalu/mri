#ifndef OREXPRESSION_HPP
#define OREXPRESSION_HPP
#include "../Expression.hpp"

class OrExpression : public Expression {
    std::unique_ptr<Expression> e1;
    std::unique_ptr<Expression> e2;

  public:
    OrExpression(Expression *e1, Expression *e2);

    ExpressionResult express(SymbolTable &st) const override;
};
#endif
