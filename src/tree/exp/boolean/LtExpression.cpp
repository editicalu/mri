#include "LtExpression.hpp"
#include "../../../error/TypeError.hpp"
#include "../../../error/UnreachableCodeError.hpp"

LtExpression::LtExpression(Expression *e1, Expression *e2) : e1(e1), e2(e2) {}

ExpressionResult LtExpression::express(SymbolTable &st) const {
    ExpressionResult r1 = this->e1->express(st);
    if (r1.index() == 1) {
        return r1;
    }
    ExpressionResult r2 = this->e2->express(st);
    if (r2.index() == 1) {
        return r2;
    }

    Value v1 = std::get<Value>(r1);
    Value v2 = std::get<Value>(r2);

    if (v1.get_type() != v2.get_type()) {
        return std::make_unique<TypeError>("<", v1.get_type(), v2.get_type());
    }

    switch (v1.get_type()) {
    case TYPE_INT:
        return Value(v1.get_int() < v2.get_int());

    case TYPE_FLOAT:
        return Value(v1.get_float() < v2.get_float());

    case TYPE_STRING:
    case TYPE_BOOL:
    case TYPE_FUNCTION:
    case TYPE_ERROR:
        return std::make_unique<TypeError>("<", v1.get_type(), v2.get_type());
    }
    return std::make_unique<UnreachableCodeError>("lttype");
}