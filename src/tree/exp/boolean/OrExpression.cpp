#include "OrExpression.hpp"
#include "../../../error/TypeError.hpp"

OrExpression::OrExpression(Expression *e1, Expression *e2) : e1(e1), e2(e2) {}

ExpressionResult OrExpression::express(SymbolTable &st) const {
    {
        ExpressionResult r1 = this->e1->express(st);
        if (r1.index() == 1) {
            return r1;
        }
        Value v1 = std::get<Value>(r1);
        if (v1.get_type() != TYPE_BOOL) {
            return std::make_unique<TypeError>("||", v1.get_type());
        }

        // Lazy evaluation
        if (v1.get_bool()) {
            return v1;
        }
    }

    {
        ExpressionResult r2 = this->e2->express(st);
        if (r2.index() == 1) {
            return r2;
        }
        Value v2 = std::get<Value>(r2);
        if (v2.get_type() != TYPE_BOOL) {
            return std::make_unique<TypeError>("||", v2.get_type());
        }

        return v2;
    }
}
