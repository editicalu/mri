#ifndef NEQEXPRESSION_HPP
#define NEQEXPRESSION_HPP

#include "../Expression.hpp"

class NEqExpression : public Expression {
    std::unique_ptr<Expression> e1;
    std::unique_ptr<Expression> e2;

  public:
    NEqExpression(Expression *e1, Expression *e2);

    ExpressionResult express(SymbolTable &st) const override;
};
#endif
