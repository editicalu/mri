#ifndef LEEXPRESSION_HPP
#define LEEXPRESSION_HPP
#include "../Expression.hpp"

class LeExpression : public Expression {
    std::unique_ptr<Expression> e1;
    std::unique_ptr<Expression> e2;

  public:
    LeExpression(Expression *e1, Expression *e2);

    virtual ExpressionResult express(SymbolTable &st) const;
};
#endif
