#include "NotExpression.hpp"
#include "../../../error/TypeError.hpp"

NotExpression::NotExpression(Expression *e1) : e1(e1) {}

ExpressionResult NotExpression::express(SymbolTable &st) const {
    ExpressionResult er = e1->express(st);
    if (er.index() == 1)
        return er;
    Value v = std::get<Value>(er);
    if (v.get_type() != TYPE_BOOL) {
        return std::make_unique<TypeError>("!", v.get_type());
    }
    return Value(!v.get_bool());
}
