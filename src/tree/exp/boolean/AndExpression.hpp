#ifndef ANDEXPRESSION_HPP
#define ANDEXPRESSION_HPP

#include "../Expression.hpp"

/// Expression that represents the boolean AND (&&). This struct ensures lazy evaluation.
class AndExpression : public Expression {
    std::unique_ptr<Expression> e1;
    std::unique_ptr<Expression> e2;

  public:
    AndExpression(Expression *e1, Expression *e2);

    ExpressionResult express(SymbolTable &st) const override;
};

#endif
