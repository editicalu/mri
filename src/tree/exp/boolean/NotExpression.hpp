#ifndef NOTEXPRESSION_HPP
#define NOTEXPRESSION_HPP

#include "../Expression.hpp"

class NotExpression : public Expression {
    std::unique_ptr<Expression> e1;

  public:
    explicit NotExpression(Expression *e1);

    ExpressionResult express(SymbolTable &st) const override;
};
#endif
