#ifndef EQEXPRESSION_HPP
#define EQEXPRESSION_HPP

#include "../Expression.hpp"

/// Expression that checks whether two expressions are the same. Works on every type, but float
/// usage is experimental.
class EqExpression : public Expression {
    std::unique_ptr<Expression> e1;
    std::unique_ptr<Expression> e2;

  public:
    EqExpression(Expression *e1, Expression *e2);

    ExpressionResult express(SymbolTable &st) const override;
};

#endif
