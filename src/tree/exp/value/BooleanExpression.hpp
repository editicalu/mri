#ifndef BOOLEANEXPRESSION_HPP
#define BOOLEANEXPRESSION_HPP

#include "../Expression.hpp"

/// BooleanExpression
// TODO: implement
class BooleanExpression : public Expression {
    bool value;

  public:
    explicit BooleanExpression(bool value);

    ExpressionResult express(SymbolTable &st) const override;
};

#endif
