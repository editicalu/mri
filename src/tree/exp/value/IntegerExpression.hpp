#ifndef INTEGEREXPRESSION_HPP
#define INTEGEREXPRESSION_HPP

#include "../Expression.hpp"

class IntegerExpression : public Expression {
  private:
    int value;

    // In case of invalid conversion, we keep the raw string for debugging purposes.
    std::optional<std::string> raw;

  public:
    explicit IntegerExpression(int value);
    explicit IntegerExpression(char const *raw_value);

    ExpressionResult express(SymbolTable &) const override;
};

#endif
