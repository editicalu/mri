#ifndef VARIABLEEXPRESSION_HPP
#define VARIABLEEXPRESSION_HPP

#include <string>

#include "../Expression.hpp"

/// Expression that uses a variable name as base.
class VariableExpression : public Expression {
  private:
    std::string variable_name;

  public:
    explicit VariableExpression(char const *variable_name);

    ExpressionResult express(SymbolTable &st) const override;
};

#endif
