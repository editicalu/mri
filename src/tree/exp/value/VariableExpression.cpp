#include "VariableExpression.hpp"

#include "../../../error/SymbolError.hpp"

VariableExpression::VariableExpression(char const *variable_name) : variable_name(variable_name) {}

ExpressionResult VariableExpression::express(SymbolTable &st) const {
    auto lookup_result = st.lookup(this->variable_name);
    if (lookup_result.has_value()) {
        // Symbol found
        return lookup_result.value();
    } else {
        // Symbol could not be found.
        return std::make_unique<SymbolError>(this->variable_name);
    }
}