#include "IntegerExpression.hpp"
#include "../../../error/InvalidNumberError.hpp"
#include <cstdlib>

IntegerExpression::IntegerExpression(int value) : value(value) {}

IntegerExpression::IntegerExpression(char const *raw_value) {
    try {
        int int_value = std::stoi(raw_value, nullptr, 10);
        this->value = int_value;
    } catch (std::invalid_argument const &) {
        // Invalid number
        this->raw = std::string(raw_value);
    } catch (std::out_of_range const &) {
        this->raw = std::string(raw_value);
    }
}

ExpressionResult IntegerExpression::express(SymbolTable &) const {
    if (this->raw.has_value()) {
        return std::make_unique<InvalidNumberError>(this->raw.value());
    } else {
        return Value(this->value);
    }
}
