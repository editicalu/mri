#include "BooleanExpression.hpp"

BooleanExpression::BooleanExpression(bool value) : value(value) {}

ExpressionResult BooleanExpression::express(SymbolTable &st) const { return Value(this->value); }
