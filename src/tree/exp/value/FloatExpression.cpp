#include "FloatExpression.hpp"
#include "../../../error/InvalidNumberError.hpp"
#include <cstdlib>

FloatExpression::FloatExpression(float value) : value(value) {}

FloatExpression::FloatExpression(char const *raw_value) {
    float parsed_value = std::strtof(raw_value, nullptr);
    if (parsed_value == 0.0F) {
        // Invalid number
        this->raw = std::string(raw_value);
    }
    this->value = parsed_value;
}

ExpressionResult FloatExpression::express(SymbolTable &) const {
    if (this->value == 0.0F) {
        return std::make_unique<InvalidNumberError>(this->raw.value());
    } else {
        return Value(this->value);
    }
}
