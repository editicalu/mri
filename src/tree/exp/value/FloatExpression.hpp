#ifndef FLOATEXPRESSION_HPP
#define FLOATEXPRESSION_HPP

#include "../Expression.hpp"

class FloatExpression : public Expression {
  private:
    float value;

    // In case of invalid conversion, we keep the raw string for debugging purposes.
    std::optional<std::string> raw;

  public:
    explicit FloatExpression(float value);
    explicit FloatExpression(char const *raw_value);

    ExpressionResult express(SymbolTable &) const override;
};

#endif
