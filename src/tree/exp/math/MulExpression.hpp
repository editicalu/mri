#ifndef MULEXPRESSION_HPP
#define MULEXPRESSION_HPP

#include "../Expression.hpp"

/// Expression to perform divisions on integers and floats
class MulExpression : public Expression {
    std::unique_ptr<Expression> e1;
    std::unique_ptr<Expression> e2;

  public:
    MulExpression(Expression *e1, Expression *e2);

    std::variant<Value, std::unique_ptr<Error>> express(SymbolTable &st) const override;
};

#endif
