#ifndef MINEXPRESSION_HPP
#define MINEXPRESSION_HPP

#include "../Expression.hpp"

class MinExpression : public Expression {
    std::unique_ptr<Expression> e1;
    std::unique_ptr<Expression> e2;

  public:
    MinExpression(Expression *e1, Expression *e2);
    virtual std::variant<Value, std::unique_ptr<Error>> express(SymbolTable &st) const;
};

#endif
