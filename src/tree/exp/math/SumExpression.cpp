#include "SumExpression.hpp"
#include "../../../error/Error.hpp"
#include "../../../error/TypeError.hpp"
#include "../../../error/UnreachableCodeError.hpp"

SumExpression::SumExpression(Expression *e1, Expression *e2)
    : e1(std::unique_ptr<Expression>(e1)), e2(e2) {}

std::variant<Value, std::unique_ptr<Error>> SumExpression::express(SymbolTable &st) const {
    auto expression_option_1 = e1->express(st);
    if (expression_option_1.index() == 1) {
        // Error occurred!
        return expression_option_1;
    }
    Value v1 = std::get<Value>(expression_option_1);

    auto expression_option_2 = e2->express(st);
    if (expression_option_2.index() == 1) {
        return expression_option_2;
    }
    Value v2 = std::get<Value>(expression_option_2);

    if (v1.get_type() != v2.get_type()) {
        // DataType error: not the same type
        return std::make_unique<TypeError>("+", v1.get_type(), v2.get_type());
    } else {
        switch (v1.get_type()) {
        case TYPE_INT: {
            // Integers
            int i1 = v1.get_int();
            int i2 = v2.get_int();

            return Value(i1 + i2);
        }
        case TYPE_FLOAT: {
            // Floats
            float f1 = v1.get_float();
            float f2 = v2.get_float();

            return Value(f1 + f2);
        }
        case TYPE_STRING: {
            // String concatenation
            std::string const &s1 = v1.get_string();
            std::string const &s2 = v2.get_string();

            return Value(s1 + s2);
        }
        case TYPE_FUNCTION:
        case TYPE_BOOL: {
            return std::make_unique<TypeError>("+", v1.get_type(), v2.get_type());
        }
        case TYPE_ERROR: {
        }
        }
        // Should be unreachable
        return std::make_unique<UnreachableCodeError>("SumExp1");
    }
}
