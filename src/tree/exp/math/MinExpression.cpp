#include "MinExpression.hpp"

#include "../../../error/Error.hpp"
#include "../../../error/TypeError.hpp"
#include "../../../error/UnreachableCodeError.hpp"

MinExpression::MinExpression(Expression *e1, Expression *e2) : e1(e1), e2(e2) {}

std::variant<Value, std::unique_ptr<Error>> MinExpression::express(SymbolTable &st) const {
    auto expression_option1 = e1->express(st);
    if (expression_option1.index() == 1) {
        // Error occurred!
        return expression_option1;
    }
    Value v1 = std::get<Value>(expression_option1);

    auto expression_option2 = e2->express(st);
    if (expression_option2.index() == 1) {
        // Error occurred!
        return expression_option2;
    }
    Value v2 = std::get<Value>(expression_option2);

    if (v1.get_type() != v2.get_type()) {
        // DataType error: not the same type
        return std::make_unique<TypeError>("-", v1.get_type(), v2.get_type());
    } else {
        switch (v1.get_type()) {
        case TYPE_INT: {
            // Integers
            int i1 = v1.get_int();
            int i2 = v2.get_int();

            return Value(i1 - i2);
        }
        case TYPE_FLOAT: {
            // Floats
            float f1 = v1.get_float();
            float f2 = v2.get_float();

            return Value(f1 - f2);
        }
        case TYPE_BOOL:
        case TYPE_FUNCTION:
        case TYPE_STRING: {
            // Strings? What is the user trying to do?
            return std::make_unique<TypeError>("-", TYPE_STRING, TYPE_STRING);
        }
        case TYPE_ERROR: {
        }
        }
        // Should be unreachable
        std::unique_ptr<Error> e = std::make_unique<UnreachableCodeError>("MinExp1");
        return std::variant<Value, std::unique_ptr<Error>>{std::move(e)};
    }
}