#include "DivExpression.hpp"
#include "../../../error/DivideByZeroError.hpp"
#include "../../../error/Error.hpp"
#include "../../../error/TypeError.hpp"
#include "../../../error/UnreachableCodeError.hpp"

DivExpression::DivExpression(Expression *e1, Expression *e2) : e1(e1), e2(e2) {}

auto DivExpression::express(SymbolTable &st) const -> ExpressionResult {
    auto expression_option_1 = e1->express(st);
    if (expression_option_1.index() == 1) {
        // Error occurred!
        return expression_option_1;
    }
    Value v1 = std::get<Value>(expression_option_1);

    auto expression_option_2 = e2->express(st);
    if (expression_option_2.index() == 1) {
        // Error occurred!
        return expression_option_2;
    }
    Value v2 = std::get<Value>(expression_option_2);

    if (v1.get_type() != v2.get_type()) {
        // Expressions do not have the same type.
        return std::make_unique<TypeError>("/", v1.get_type(), v2.get_type());
    } else {
        switch (v1.get_type()) {
        case TYPE_INT: {
            // Integers
            int i1 = v1.get_int();
            int i2 = v2.get_int();

            if (i2 == 0) {
                return std::make_unique<DivideByZeroError>();
            }

            return Value(i1 / i2);
        }
        case TYPE_FLOAT: {
            // Floats
            float f1 = v1.get_float();
            float f2 = v2.get_float();

            // TODO: verify this
            if (f2 == 0.0) {
                return std::make_unique<DivideByZeroError>();
            }

            return Value(f1 / f2);
        }
        case TYPE_BOOL:
        case TYPE_FUNCTION:
        case TYPE_STRING: {
            // String? What are you trying to do?
            return std::make_unique<TypeError>("/", v1.get_type(), v2.get_type());
        }
        case TYPE_ERROR: {
        }
        }
        // Should be unreachable
        std::unique_ptr<Error> e = std::make_unique<UnreachableCodeError>("SumExp1");
        return std::variant<Value, std::unique_ptr<Error>>{std::move(e)};
    }
}