#ifndef SUMEXPRESSION_HPP
#define SUMEXPRESSION_HPP

#include "../Expression.hpp"
#include <memory>

class SumExpression : public Expression {
    std::unique_ptr<Expression> e1;
    std::unique_ptr<Expression> e2;

  public:
    SumExpression(Expression *e1, Expression *e2);
    std::variant<Value, std::unique_ptr<Error>> express(SymbolTable &st) const override;
};

#endif
