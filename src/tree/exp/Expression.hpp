#ifndef EXPRESSION_HPP
#define EXPRESSION_HPP

#include <memory>
#include <variant>

#include "../../data/SymbolTable.hpp"
#include "../../data/Value.hpp"
#include "../../error/Error.hpp"

class Value;

typedef std::variant<Value, std::unique_ptr<Error>> ExpressionResult;

/// Represents an expression. The type of this expression is not defined.
/// Returns:
/// - TypeError if there's a type error somewhere in your program.
class Expression {
  public:
    [[nodiscard]] virtual ExpressionResult express(SymbolTable &) const = 0;
};

#endif
