#ifndef ASSIGNMENTEXPRESSION_HPP
#define ASSIGNMENTEXPRESSION_HPP

#include "Expression.hpp"

class AssignmentExpression : public Expression {
    std::string name;
    std::unique_ptr<Expression> expression;

  public:
    AssignmentExpression(char const *name, Expression *expression);

    [[nodiscard]] virtual ExpressionResult express(SymbolTable &st) const;
};

#endif
