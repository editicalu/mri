#include "FunctionCallExpression.hpp"
#include "../../../data/function/UserFunction.hpp"
#include "../../../error/SymbolError.hpp"
#include "../../../error/TypeError.hpp"

FunctionCallExpression::FunctionCallExpression(char const *function_name, ArgListConstructor *alc)
    : function_name(function_name), args(std::make_unique<ArgList>(alc)) {}

ExpressionResult FunctionCallExpression::express(SymbolTable &st) const {
    // Find the function
    auto const &function_opt = st.lookup(this->function_name);
    if (!function_opt.has_value()) {
        // No such function exists.
        return std::make_unique<SymbolError>(this->function_name);
    }
    Value v = function_opt.value();
    if (v.get_type() != TYPE_FUNCTION) {
        // Symbol is not a function.
        return std::make_unique<TypeError>("functioncall", v.get_type());
    }
    Function const &function = *v.get_function();
    // See if it returned something
    ArgList const &args = *this->args;
    auto f_result = function.run(st, args, this->function_name);
    if (f_result.index() == 1) {
        // Error occurred
        return std::get<std::unique_ptr<Error>>(std::move(f_result));
    } else {
        auto v = std::get<std::optional<Value>>(std::move(f_result));
        if (v.has_value()) {
            return v.value();
        } else {
            // Default return value. We could make the program better by allowing a Value to be null
            // or some other explicit indicator of the fact that this function does not return
            // anything.
            return Value(0);
        }
    }
}
