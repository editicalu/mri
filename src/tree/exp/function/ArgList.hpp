#ifndef ARGLIST_HPP
#define ARGLIST_HPP

#include "../Expression.hpp"
#include "ArgListConstructor.hpp"
#include <memory>
#include <vector>

/// The ArgList is a concrete instance of the signature of a function.
class ArgList {
    std::vector<std::unique_ptr<Expression>> arg_list;

  public:
    explicit ArgList(ArgListConstructor *alc);
    std::optional<std::unique_ptr<Error>> add_to_symbol_table(
        SymbolTable &st,
        std::vector<std::unique_ptr<std::string>> const &names,
        std::string const &function_name) const;
};

#endif
