#include "ArgListConstructor.hpp"

ArgListConstructor::ArgListConstructor(Expression *exp, ArgListConstructor *next)
    : argument(exp), next(next) {}

std::vector<std::unique_ptr<Expression>> ArgListConstructor::into_vector() {
    std::vector<std::unique_ptr<Expression>> list;
    list.push_back(std::move(this->argument));
    std::unique_ptr<ArgListConstructor> next_one = std::move(this->next);
    while (next_one != nullptr) {
        list.push_back(std::move(next_one->argument));
        next_one = std::move(next_one->next);
    }
    return list;
}
