#ifndef ARGLISTCONSTRUCTOR_HPP
#define ARGLISTCONSTRUCTOR_HPP

#include "../../exp/Expression.hpp"
#include <memory>
#include <string>

class ArgListConstructor {
    std::unique_ptr<Expression> argument;
    std::unique_ptr<ArgListConstructor> next;

  public:
    explicit ArgListConstructor(Expression *exp, ArgListConstructor *next = nullptr);

    std::vector<std::unique_ptr<Expression>> into_vector();
};

#endif
