#include "ArgList.hpp"
#include "../../../error/FunctionCallError.hpp"
#include "../../../error/UnreachableCodeError.hpp"

ArgList::ArgList(ArgListConstructor *alc)
    : arg_list(
          alc == nullptr ? std::vector<std::unique_ptr<Expression>>()
                         : std::move(alc->into_vector())) { /*delete alc; */
}

std::optional<std::unique_ptr<Error>> ArgList::add_to_symbol_table(
    SymbolTable &st,
    std::vector<std::unique_ptr<std::string>> const &names,
    std::string const &function_name) const {

    SymbolTable &source = *st.get_next().value();
    if (this->arg_list.size() != names.size()) {
        return std::make_unique<FunctionCallError>(
            this->arg_list.size(), names.size(), function_name);
    }
    for (size_t index = 0; index < this->arg_list.size(); ++index) {
        auto const &expression = this->arg_list[index];
        auto const &name = names[index];

        ExpressionResult er = expression->express(source);
        switch (er.index()) {
        case 0: {
            Value v = std::get<Value>(er);
            if (st.force_insert(*name, v)) {
                // Normally, we checked the variable names at the definition of the function.
                return std::make_unique<UnreachableCodeError>("arglist_alreadyinserted");
            } else {
                continue;
            }
        }
        case 1: {
            // Error when expressing.
            return std::optional{std::get<std::unique_ptr<Error>>(std::move(er))};
        }
        }
        return std::make_unique<UnreachableCodeError>("arglistbottom");
    }
    return std::nullopt;
}
