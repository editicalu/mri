#ifndef FUNCTIONCALLEXPRESSION_HPP
#define FUNCTIONCALLEXPRESSION_HPP

#include "../Expression.hpp"
#include <string>

#include "ArgList.hpp"
#include "ArgListConstructor.hpp"

class FunctionCallExpression : public Expression {
    std::string function_name;
    std::unique_ptr<ArgList> args;

  public:
    FunctionCallExpression(char const *function_name, ArgListConstructor *alc);

    ExpressionResult express(SymbolTable &st) const override;
};

#endif
