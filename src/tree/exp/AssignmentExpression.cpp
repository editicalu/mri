#include "AssignmentExpression.hpp"

ExpressionResult AssignmentExpression::express(SymbolTable &st) const {
    ExpressionResult er = this->expression->express(st);
    if (er.index() != 1) {
        Value v = std::get<Value>(er);
        st.update_or_insert(this->name, v);
    }
    return er;
}

AssignmentExpression::AssignmentExpression(char const *name, Expression *expression)
    : name(name), expression(expression) {}
