#ifndef VALUE_HPP
#define VALUE_HPP

#include "DataType.hpp"

#include <memory>
#include <string>
#include <variant>

class Function;
typedef std::variant<int, float, std::string, bool, Function *> VariantValue;

/// Represents the result of expressing an expression.
class Value {
  private:
    VariantValue value;

  public:
    /// Creates a new Value with the given value.
    explicit Value(VariantValue value);

    /// Creates a new Value that holds the given integer.
    explicit Value(int value);

    /// Creates a new Value that holds the given floating point number.
    explicit Value(float value);

    /// Creates a new Value that holds the given string.
    explicit Value(std::string value);

    /// Creates a new Value that holds the given boolean.
    explicit Value(bool value);

    /// Creates a new Value that holds the given function. Does not take ownership over the function
    /// and assumes that this function will keep living for the duration of the Value.
    explicit Value(Function *value);

    [[nodiscard]] DataType get_type() const;

    /// Returns the integer value out of this value. Crashes if this value does not hold an integer.
    [[nodiscard]] int get_int() const;

    /// Returns the float value out of this value. Crashes if this value does not hold an float.
    [[nodiscard]] float get_float() const;

    /// Returns the boolean value out of this value. Crashes if this value does not hold a boolean.
    [[nodiscard]] bool get_bool() const;

    /// Returns the string out of this value. Crashes if this value does not hold a string.
    [[nodiscard]] std::string const &get_string() const;

    /// Returns the function pointer of this value. Crashes if this value does not hold a function.
    [[nodiscard]] Function const *get_function() const;

    /// Prints the type and the value of this value to stdout.
    void print() const;

    /// Prints the value of this value to stdout.
    void print_value() const;
};

#endif
