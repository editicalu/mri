#ifndef SYMBOLTABLE_HPP
#define SYMBOLTABLE_HPP

#include <memory>
#include <optional>
#include <vector>

#include "Variable.hpp"

/// SymbolTable keeps the symbols and their values for the program.
class SymbolTable {
  private:
    /// Values in this SymbolTable.
    std::vector<Variable> values;
    /// A possible next symboltable. They work like following stack frames.
    SymbolTable *next;

  protected:
    /// Tries to find the name in the current symboltable and update it if it exists.
    /// Does nothing if the name does not exists.
    /// Returns whether name exists.
    bool update(std::string const &name, Value const &new_value);

  public:
    /// Creates a new SymbolTable with the given one as the next one. If none is given (default),
    /// the symbol table will be completely empty.
    explicit SymbolTable(SymbolTable *next = nullptr);

    /// Tries to find a value with the given name. Returns a option with a pointer to the value if
    /// found, returns std::nullopt otherwise. This method will search for the value in the next
    /// symbol tables.
    [[nodiscard]] std::optional<Value> lookup(std::string const &name) const;

    /// Tries to insert a new value in the symbol table with the given name. If a variable exists
    /// with the same name, its value shall be overwritten. Returns whether an existing variable is
    /// updated.
    bool update_or_insert(std::string const &name, Value const &value);

    /// Will insert the name of the variable in the current symbol table frame. This will not update
    /// previous values. This function should be used when inserting parameters for functions into
    /// the symbol table, as you don't want them to override your current ones with the same names.
    /// Returns whether a variable with that name exists in the current frame. It will be
    /// overwritten in that case.
    bool force_insert(std::string const &name, Value const &value);

    /// Deletes the variable with the given name from the symbol table. This function will work
    /// recursively if required.
    bool delete_name(std::string const &name);

    /// Prints the contents of this symboltable to stderr. Useful for debugging or when the program
    /// is done executing.
    void print() const;

    /// Returns the next SymbolTable. It would be nice not to call any destructing things on this
    /// instance. The pointer should only be used for lookups, updates or insertions.
    [[nodiscard]] std::optional<SymbolTable *> get_next() const {
        return this->next == nullptr ? std::nullopt : std::optional{this->next};
    }
};

#endif