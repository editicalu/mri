#include "SymbolTable.hpp"
#include <iostream>

SymbolTable::SymbolTable(SymbolTable *next) : next(next) {}

std::optional<Value> SymbolTable::lookup(std::string const &name) const {
    for (auto value : this->values) {
        // Search in this symbol table.
        if (value.name == name) {
            return std::optional{value.value};
        }
    }
    // search in the next symbol table.
    if (this->next == nullptr) {
        return std::nullopt;
    } else {
        return this->next->lookup(name);
    }
}

bool SymbolTable::update(std::string const &name, Value const &new_value) {
    for (auto &variable : this->values) {
        if (variable.name == name) {
            variable.value = new_value;
            return true;
        }
    }
    if (this->next != nullptr) {
        return this->next->update(name, new_value);
    } else {
        return false;
    }
}

bool SymbolTable::update_or_insert(std::string const &name, Value const &new_value) {
    if (this->update(name, new_value))
        return true;
    this->values.emplace_back(name, new_value);
    return false;
}

void SymbolTable::print() const {
    std::cerr << "SymbolTable" << std::endl;
    for (auto const &var : this->values) {
        std::cerr << var.name << "\t";
        var.value.print();
    }
}

bool SymbolTable::delete_name(const std::string &name) {
    for (size_t idx = 0; idx < this->values.size(); ++idx) {
        if (this->values.at(idx).name == name) {
            this->values.erase(this->values.begin() + idx);
            return true;
        }
    }
    if (this->next == nullptr)
        return false;
    else
        return this->next->delete_name(name);
}

bool SymbolTable::force_insert(std::string const &name, Value const &value) {
    for (auto &var : this->values) {
        if (var.name == name) {
            // Override existing variable in frame
            var.value = value;
            return true;
        }
    }
    // Does not exists. Inserting a new value
    this->values.emplace_back(name, value);
    return false;
}
