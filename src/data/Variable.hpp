#ifndef VARIABLE_HPP
#define VARIABLE_HPP

#include <memory>
#include <string>
#include <variant>
#include <vector>

#include "DataType.hpp"
#include "Value.hpp"

/// Value with an identifier.
/// Stores one variable in the symbol table. It allows for different types of memory to be stored.
class Variable {
  public:
    Variable(std::string name, Value value);

    /// Name of the variable
    std::string name;
    /// This variable contains a pointer to the actual value of this variable.
    Value value;

    /// Creates a new Variable with an integer in it.
    static Variable new_int(std::string name, int value);

    /// Creates a new Variable with a string in it.
    static Variable new_string(std::string name, std::string value);

    /// Creates a new Variable with a float in it.
    static Variable new_float(std::string name, float value);

    /// Creates a new Variable with a boolean in it.
    static Variable new_bool(std::string name, bool value);
};

#endif
