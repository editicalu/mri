#ifndef DATATYPE_HPP
#define DATATYPE_HPP

#include <string>

/// DataType is a enum that indicates which type of data is stored in a given Value. It should never
/// be TYPE_ERROR (unless something went wrong).
enum DataType {
    TYPE_INT = 0,
    TYPE_FLOAT = 1,
    TYPE_STRING = 2,
    TYPE_BOOL = 3,
    TYPE_FUNCTION = 4,
    TYPE_ERROR = 99
};

typedef enum DataType DataType;

std::string datatype_to_string(DataType t);

#endif
