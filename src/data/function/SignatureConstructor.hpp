#ifndef SIGNATURECONSTRUCTOR_HPP
#define SIGNATURECONSTRUCTOR_HPP

#include <memory>
#include <string>
#include <vector>

/// Helper function to create signatures from Yacc.
class SignatureConstructor {
  private:
    /// Name of this variable in the signature. Should not be nullptr.
    std::unique_ptr<std::string> name;
    /// Possible pointer to the next item in a signature. Can be nullptr.
    std::unique_ptr<SignatureConstructor> next;

  public:
    explicit SignatureConstructor(char const *name, SignatureConstructor *sc = nullptr);

    /// Converts this SignatureConstructor and all of its children to a vector. This should be
    /// called only once. Deallocates all the children that are no longer required.
    std::vector<std::unique_ptr<std::string>> into_list();
};

#endif
