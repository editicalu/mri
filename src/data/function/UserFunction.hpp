#ifndef USERFUNCTION_HPP
#define USERFUNCTION_HPP

#include "../../tree/exp/Expression.hpp"
#include "../../tree/exp/function/ArgList.hpp"
#include "../../tree/stm/BlockStatement.hpp"
#include "Function.hpp"
#include "SignatureConstructor.hpp"

/// The UserFunction class represents a function created by a user (in our miniRuby, as opposed to
/// the functions in the standard library).
class UserFunction : public Function {
    /// Body of the function
    std::unique_ptr<BlockStatement> function;
    /// Name of arguments that should be passed to this function.
    std::vector<std::unique_ptr<std::string>> signature;

  public:
    UserFunction(SignatureConstructor *sc, CmpStatement *cs);

    [[nodiscard]] std::vector<std::unique_ptr<std::string>> const &get_signature() const;

    /// Tries to run the function with the given arguments.
    StatementResult
    run(SymbolTable &st, ArgList const &arguments, std::string const &function_name) const override;
};

#endif
