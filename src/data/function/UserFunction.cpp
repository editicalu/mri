#include "UserFunction.hpp"

UserFunction::UserFunction(SignatureConstructor *sc, CmpStatement *cs)
    : function(std::make_unique<BlockStatement>(cs)),
      signature(sc == nullptr ? std::vector<std::unique_ptr<std::string>>(0) : sc->into_list()) {

    delete sc;
}

std::vector<std::unique_ptr<std::string>> const &UserFunction::get_signature() const {
    return this->signature;
}

std::variant<ReturnValue, std::unique_ptr<Error>> UserFunction::run(
    SymbolTable &st,
    const ArgList &arguments,
    std::string const &function_name) const {
    SymbolTable new_st(&st);
    arguments.add_to_symbol_table(new_st, this->signature, function_name);
    return this->function->execute(new_st, true);
}
