#include "SignatureConstructor.hpp"
#include <optional>

SignatureConstructor::SignatureConstructor(char const *name, SignatureConstructor *sc)
    : name(std::make_unique<std::string>(name)), next(sc) {}

std::vector<std::unique_ptr<std::string>> SignatureConstructor::into_list() {
    std::vector<std::unique_ptr<std::string>> list;
    list.push_back(std::move(this->name));
    std::unique_ptr<SignatureConstructor> signature = std::move(this->next);
    while (signature != nullptr) {
        list.push_back(std::move(signature->name));
        signature = std::move(signature->next);
    }
    return list;
}
