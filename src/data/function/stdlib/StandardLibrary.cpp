#include "StandardLibrary.hpp"

StandardLibrary::StandardLibrary() : print_function(std::make_unique<PrintFunction>()) {}

void StandardLibrary::add_to_symbol_table(SymbolTable &st) const {
    st.force_insert("print", Value(this->print_function.get()));
}
