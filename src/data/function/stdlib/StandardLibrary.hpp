#ifndef STANDARDLIBRARY_HPP
#define STANDARDLIBRARY_HPP

#include <memory>
#include <optional>
#include <utility>
#include <vector>

#include "../../SymbolTable.hpp"
#include "../../Value.hpp"

#include "PrintFunction.hpp"

/// The StandardLibrary contains all the functions that are builtin into the Ruby language. These
/// can be any value (e.g. pi), but also a functions (e.g. print). These values should be inserted
/// in a SymbolTable using add_to_symbol_table.
class StandardLibrary {
  public:
    /// Create a new StandardLibrary.
    StandardLibrary();

    /// The print function that is in the standard library.
    std::unique_ptr<PrintFunction> print_function;

    /// Add the standard library to a symbol table. This should be done at the highest level Symbol
    /// Table. For memory safety, this object should outlive the Symbol Table it is inserted into.
    void add_to_symbol_table(SymbolTable &st) const;
};

#endif
