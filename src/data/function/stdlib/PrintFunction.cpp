#include "PrintFunction.hpp"
#include "../../../error/SymbolError.hpp"

PrintFunction::PrintFunction() { arglist.push_back(std::make_unique<std::string>("printable")); }

StatementResult PrintFunction::run(
    SymbolTable &st,
    ArgList const &arguments,
    std::string const &function_name) const {
    SymbolTable new_st(&st);
    auto err_opt = arguments.add_to_symbol_table(new_st, this->arglist, std::string("print"));
    if (err_opt.has_value()) {
        return std::move(err_opt.value());
    }
    std::optional<Value> v_opt = new_st.lookup("printable");
    if (!v_opt.has_value()) {
        return std::make_unique<SymbolError>("printable");
    }
    v_opt.value().print_value();
    return std::nullopt;
}
