#ifndef PRINTFUNCTION_HPP
#define PRINTFUNCTION_HPP

#include "../UserFunction.hpp"

/// Creates the print() function in the standard library. Prints to the stdout. Printing to any
/// other output is not supported.
class PrintFunction : public Function {
  private:
    // arglist, required by the symboltable in order to define it.
    std::vector<std::unique_ptr<std::string>> arglist;

  public:
    PrintFunction();

    StatementResult
    run(SymbolTable &st, ArgList const &arguments, std::string const &function_name) const override;
};

#endif
