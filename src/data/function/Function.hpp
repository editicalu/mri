#ifndef FUNCTION_HPP
#define FUNCTION_HPP
/// Function is an interface-like abstract class that represents functions callable in Ruby. These
/// functions can be both defined by the user or available in the standard library.
class Function {
  public:
    /// Tries to run the function with the given arguments.
    virtual StatementResult
    run(SymbolTable &st, ArgList const &arguments, std::string const &function_name) const = 0;
};

#endif
