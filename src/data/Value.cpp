#include "Value.hpp"
#include <iostream>
#include <utility>

Value::Value(VariantValue value) : value(std::move(std::move(value))) {}

Value::Value(int value) : value(value) {}

Value::Value(float value) : value(value) {}

Value::Value(bool value) : value(value) {}

Value::Value(std::string value) : value(value) {}

Value::Value(Function *value) : value(value) {}

DataType Value::get_type() const {
    switch (this->value.index()) {
    case 0:
        return TYPE_INT;
    case 1:
        return TYPE_FLOAT;
    case 2:
        return TYPE_STRING;
    case 3:
        return TYPE_BOOL;
    case 4:
        return TYPE_FUNCTION;
    }
    // Should not happen
    return TYPE_ERROR;
}

int Value::get_int() const { return std::get<int>(this->value); }

float Value::get_float() const { return std::get<float>(this->value); }

bool Value::get_bool() const { return std::get<bool>(this->value); }

std::string const &Value::get_string() const { return std::get<std::string>(this->value); }

Function const *Value::get_function() const { return std::get<Function *>(this->value); }

void Value::print() const {
    switch (this->get_type()) {
    case TYPE_INT: {
        std::cerr << "integer\t\t" << std::to_string(this->get_int());
        break;
    }
    case TYPE_FLOAT: {
        std::cerr << "float\t\t" << this->get_float();
        break;
    }
    case TYPE_STRING: {
        std::cerr << "string\t\t" << this->get_string();
        break;
    }
    case TYPE_BOOL: {
        std::cerr << "bool\t\t";
        if (this->get_bool()) {
            std::cerr << "true";
        } else {
            std::cerr << "false";
        }
        break;
    }
    case TYPE_FUNCTION: {
        std::cerr << "function\t";
        std::cerr << "<ptr to " << std::hex << this->get_function();
        std::cerr << ">";
        break;
    }
    case TYPE_ERROR: {
        std::cerr << "type_error";
        break;
    }
    }
    std::cerr << std::endl;
}

void Value::print_value() const {
    switch (this->get_type()) {
    case TYPE_INT: {
        std::cout << std::to_string(this->get_int());
        break;
    }
    case TYPE_FLOAT: {
        std::cout << this->get_float();
        break;
    }
    case TYPE_STRING: {
        std::cout << this->get_string();
        break;
    }
    case TYPE_BOOL: {
        if (this->get_bool()) {
            std::cout << "true";
        } else {
            std::cout << "false";
        }
        break;
    }
    case TYPE_FUNCTION: {
        std::cout << "function\t";
        std::cout << "<ptr to " << std::hex << this->get_function();
        std::cout << ">";
        break;
    }
    case TYPE_ERROR: {
        std::cout << "type_error";
        break;
    }
    }
    std::cout << std::endl;
}
