#include "Variable.hpp"

#include <utility>

#include <utility>

Variable::Variable(std::string name, Value value)
    : name(std::move(std::move(name))), value(std::move(std::move(value))) {}

Variable Variable::new_float(std::string name, float value) {
    return Variable(std::move(name), Value(value));
}

Variable Variable::new_int(std::string name, int value) {
    return Variable(std::move(name), Value(value));
}

Variable Variable::new_string(std::string name, std::string value) {
    return Variable(std::move(name), Value(std::move(value)));
}

Variable Variable::new_bool(std::string name, bool value) {
    return Variable(std::move(name), Value(value));
}
