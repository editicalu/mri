#include "DataType.hpp"

std::string datatype_to_string(DataType t) {
    switch (t) {
    case TYPE_INT:
        return "int";
    case TYPE_FLOAT:
        return "float";
    case TYPE_STRING:
        return "string";
    case TYPE_BOOL:
        return "bool";
    case TYPE_FUNCTION:
        return "function*";
    case TYPE_ERROR:
        return "<type error>";
    }
    return "<type error>";
}