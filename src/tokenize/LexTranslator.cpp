#include "LexTranslator.hpp"

std::optional<TokenType> lex_index_to_tokentype(int lex_value) {
    switch (lex_value) {
    case LEX_BINOP_PLUS:
        return std::optional{LEX_BINOP_PLUS};
    case LEX_BINOP_MINUS:
        return std::optional{LEX_BINOP_MINUS};
    case LEX_BINOP_TIMES:
        return std::optional{LEX_BINOP_TIMES};
    case LEX_BINOP_DIV:
        return std::optional{LEX_BINOP_DIV};
    case LEX_BINOP_ISEQ:
        return std::optional{LEX_BINOP_ISEQ};
    case LEX_BINOP_ISNOTEQ:
        return std::optional{LEX_BINOP_ISNOTEQ};
    case LEX_BINOP_GT:
        return std::optional{LEX_BINOP_GT};
    case LEX_BINOP_GE:
        return std::optional{LEX_BINOP_GE};
    case LEX_BINOP_LT:
        return std::optional{LEX_BINOP_LT};
    case LEX_BINOP_LE:
        return std::optional{LEX_BINOP_LE};
    case LEX_BINOP_AND:
        return std::optional{LEX_BINOP_AND};
    case LEX_BINOP_OR:
        return std::optional{LEX_BINOP_OR};

    case LEX_ASSIGN_EQ:
        return std::optional{LEX_ASSIGN_EQ};
    case LEX_ASSIGN_PLUSEQ:
        return std::optional{LEX_ASSIGN_PLUSEQ};
    case LEX_ASSIGN_MINUSEQ:
        return std::optional{LEX_ASSIGN_MINUSEQ};
    case LEX_ASSIGN_TIMESEQ:
        return std::optional{LEX_ASSIGN_TIMESEQ};
    case LEX_ASSIGN_DIVEQ:
        return std::optional{LEX_ASSIGN_DIVEQ};
    case LEX_ASSIGN_ANDEQ:
        return std::optional{LEX_ASSIGN_ANDEQ};
    case LEX_ASSIGN_OREQ:
        return std::optional{LEX_ASSIGN_OREQ};

    case LEX_TERM_NEWLINE:
        return std::optional{LEX_TERM_NEWLINE};
    case LEX_TERM_SEMICOLON:
        return std::optional{LEX_TERM_SEMICOLON};

    case LEX_SYM_IDENTIFIER:
        return std::optional{LEX_SYM_IDENTIFIER};
    case LEX_SYM_INTEGER:
        return std::optional{LEX_SYM_INTEGER};
    case LEX_SYM_FLOAT:
        return std::optional{LEX_SYM_FLOAT};

    case LEX_KEYWORD_UNDEF:
        return std::optional{LEX_KEYWORD_UNDEF};
    case LEX_KEYWORD_DEF:
        return std::optional{LEX_KEYWORD_DEF};
    case LEX_KEYWORD_END:
        return std::optional{LEX_KEYWORD_END};
    case LEX_KEYWORD_THEN:
        return std::optional{LEX_KEYWORD_THEN};
    case LEX_KEYWORD_IF:
        return std::optional{LEX_KEYWORD_IF};
    case LEX_KEYWORD_ELSIF:
        return std::optional{LEX_KEYWORD_ELSIF};
    case LEX_KEYWORD_ELSE:
        return std::optional{LEX_KEYWORD_ELSE};
    case LEX_KEYWORD_UNLESS:
        return std::optional{LEX_KEYWORD_UNLESS};
    case LEX_KEYWORD_WHILE:
        return std::optional{LEX_KEYWORD_WHILE};
    case LEX_KEYWORD_CASE:
        return std::optional{LEX_KEYWORD_CASE};
    case LEX_KEYWORD_WHEN:
        return std::optional{LEX_KEYWORD_WHEN};
    case LEX_KEYWORD_DO:
        return std::optional{LEX_KEYWORD_DO};
    case LEX_KEYWORD_TRUE:
        return std::optional{LEX_KEYWORD_TRUE};
    case LEX_KEYWORD_FALSE:
        return std::optional{LEX_KEYWORD_FALSE};
    case LEX_KEYWORD_RETURN:
        return std::optional{LEX_KEYWORD_RETURN};
    case LEX_KEYWORD_UNTIL:
        return std::optional{LEX_KEYWORD_UNTIL};

    case LEX_CHAR_COMMA:
        return std::optional{LEX_CHAR_COMMA};
    case LEX_CHAR_NOT:
        return std::optional{LEX_CHAR_NOT};

    case LEX_PAIRS_LPAR:
        return std::optional{LEX_PAIRS_LPAR};
    case LEX_PAIRS_RPAR:
        return std::optional{LEX_PAIRS_RPAR};
    case LEX_PAIRS_LBRA:
        return std::optional{LEX_PAIRS_LBRA};
    case LEX_PAIRS_RBRA:
        return std::optional{LEX_PAIRS_RBRA};

    case LEX_NOP_COMMENT:
        return std::optional{LEX_NOP_COMMENT};
    case LEX_NOP_WHITESPACE:
        return std::optional{LEX_NOP_WHITESPACE};
    }
    return std::nullopt;
}
