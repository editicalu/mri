#include "Token.hpp"
#include <iostream>

Token::Token(TokenType token_id, std::string raw_contents, size_t row, size_t col)
    : token_id(token_id), raw_contents(raw_contents), row(row), col(col) {}

void Token::print_debug() const {
    std::cerr << get_tokentype_meaning(this->token_id) << "(" << this->raw_contents << ") ";

    if (this->token_id == LEX_TERM_NEWLINE) {
        std::cerr << std::endl;
    }
}

TokenType Token::get_token_type() const { return this->token_id; }

std::string const &Token::get_raw() const { return this->raw_contents; }

std::string get_tokentype_meaning(TokenType tt) {
    switch (tt) {
    case LEX_BINOP_PLUS:
        return std::string("LEX_BINOP_PLUS");
    case LEX_BINOP_MINUS:
        return std::string("LEX_BINOP_MINUS");
    case LEX_BINOP_TIMES:
        return std::string("LEX_BINOP_TIMES");
    case LEX_BINOP_DIV:
        return std::string("LEX_BINOP_DIV");
    case LEX_BINOP_ISEQ:
        return std::string("LEX_BINOP_ISEQ");
    case LEX_BINOP_ISNOTEQ:
        return std::string("LEX_BINOP_ISNOTEQ");
    case LEX_BINOP_GT:
        return std::string("LEX_BINOP_GT");
    case LEX_BINOP_GE:
        return std::string("LEX_BINOP_GE");
    case LEX_BINOP_LT:
        return std::string("LEX_BINOP_LT");
    case LEX_BINOP_LE:
        return std::string("LEX_BINOP_LE");
    case LEX_BINOP_AND:
        return std::string("LEX_BINOP_AND");
    case LEX_BINOP_OR:
        return std::string("LEX_BINOP_OR");

    case LEX_ASSIGN_EQ:
        return std::string("LEX_ASSIGN_EQ");
    case LEX_ASSIGN_PLUSEQ:
        return std::string("LEX_ASSIGN_PLUSEQ");
    case LEX_ASSIGN_MINUSEQ:
        return std::string("LEX_ASSIGN_MINUSEQ");
    case LEX_ASSIGN_TIMESEQ:
        return std::string("LEX_ASSIGN_TIMESEQ");
    case LEX_ASSIGN_DIVEQ:
        return std::string("LEX_ASSIGN_DIVEQ");
    case LEX_ASSIGN_ANDEQ:
        return std::string("LEX_ASSIGN_ANDEQ");
    case LEX_ASSIGN_OREQ:
        return std::string("LEX_ASSIGN_OREQ");

    case LEX_TERM_NEWLINE:
        return std::string("LEX_TERM_NEWLINE");
    case LEX_TERM_SEMICOLON:
        return std::string("LEX_TERM_SEMICOLON");

    case LEX_SYM_IDENTIFIER:
        return std::string("LEX_SYM_IDENTIFIER");
    case LEX_SYM_INTEGER:
        return std::string("LEX_SYM_INTEGER");
    case LEX_SYM_FLOAT:
        return std::string("LEX_SYM_FLOAT");

    case LEX_KEYWORD_UNDEF:
        return std::string("LEX_KEYWORD_UNDEF");
    case LEX_KEYWORD_DEF:
        return std::string("LEX_KEYWORD_DEF");
    case LEX_KEYWORD_END:
        return std::string("LEX_KEYWORD_END");
    case LEX_KEYWORD_THEN:
        return std::string("LEX_KEYWORD_THEN");
    case LEX_KEYWORD_IF:
        return std::string("LEX_KEYWORD_IF");
    case LEX_KEYWORD_ELSIF:
        return std::string("LEX_KEYWORD_ELSIF");
    case LEX_KEYWORD_ELSE:
        return std::string("LEX_KEYWORD_ELSE");
    case LEX_KEYWORD_UNLESS:
        return std::string("LEX_KEYWORD_UNLESS");
    case LEX_KEYWORD_WHILE:
        return std::string("LEX_KEYWORD_WHILE");
    case LEX_KEYWORD_CASE:
        return std::string("LEX_KEYWORD_CASE");
    case LEX_KEYWORD_WHEN:
        return std::string("LEX_KEYWORD_WHEN");
    case LEX_KEYWORD_DO:
        return std::string("LEX_KEYWORD_DO");
    case LEX_KEYWORD_TRUE:
        return std::string("LEX_KEYWORD_TRUE");
    case LEX_KEYWORD_FALSE:
        return std::string("LEX_KEYWORD_FALSE");
    case LEX_KEYWORD_RETURN:
        return std::string("LEX_KEYWORD_RETURN");
    case LEX_KEYWORD_UNTIL:
        return std::string("LEX_KEYWORD_UNTIL");

    case LEX_CHAR_COMMA:
        return std::string("LEX_CHAR_COMMA");
    case LEX_CHAR_NOT:
        return std::string("LEX_CHAR_NOT");

    case LEX_PAIRS_LPAR:
        return std::string("LEX_PAIRS_LPAR");
    case LEX_PAIRS_RPAR:
        return std::string("LEX_PAIRS_RPAR");
    case LEX_PAIRS_LBRA:
        return std::string("LEX_PAIRS_LBRA");
    case LEX_PAIRS_RBRA:
        return std::string("LEX_PAIRS_RBRA");

    case LEX_NOP_WHITESPACE:
        return std::string("LEX_NOP_WHITESPACE");
    case LEX_NOP_COMMENT:
        return std::string("LEX_NOP_COMMENT");
    }
    return std::string("??");
}

size_t Token::get_row() const { return this->row; }

size_t Token::get_col() const { return this->col; }
