#pragma once

#ifndef TOKENSTRING_HPP
#define TOKENSTRING_HPP

#include <memory>
#include <optional>
#include <variant>
#include <vector>

#include "../error/IOError.hpp"
#include "../error/TokenizeError.hpp"
#include "Token.hpp"

/// Represents a file that is tokenized.
class TokenString {
  private:
    TokenString(std::vector<Token>);

    static std::variant<TokenString, std::unique_ptr<Error>> try_tokenize(FILE *source);

  public:
    /// The actual tokens in the parsed file
    std::vector<Token> tokens;

    /// Returns TokenString if the file (path given as parameter) contains only correct tokens.
    /// Returns a ParsingError otherwise.
    static std::variant<TokenString, std::unique_ptr<Error>> try_tokenize_file(std::string const &);

    /// Returns TokenString if the stdin contains only correct tokens. Returns a ParsingError
    /// otherwise.
    static std::variant<TokenString, std::unique_ptr<Error>> try_tokenize_stdin();

    /// Removes every whitespace and comment from the tokenstring.
    void clean();

    /// Print the contents of this TokenString to stderr. This function should be used for debugging
    /// purposes.
    void print_debug() const;
};

#endif