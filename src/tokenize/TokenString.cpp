#include "TokenString.hpp"

#include "LexTranslator.hpp"
#include "Token.hpp"
#include "TokenType.h"
#include "lex.yy.h"
#include <iostream>

TokenString::TokenString(std::vector<Token> tokens) : tokens(tokens) {}

std::variant<TokenString, std::unique_ptr<Error>> TokenString::try_tokenize(FILE *source) {
    std::vector<Token> tokens;
    size_t col_position = 1, row_position = 1;
    int new_token;
    /* If we de not explicitly bind yyin to a file, stdin is assumed. */

    if (source != nullptr) {
        yyin = source;
    }

    new_token = yylex();
    while (new_token != 0) {
        /* Append value */
        std::optional<TokenType> tt = lex_index_to_tokentype(new_token);
        if (tt.has_value()) {
            // We have a invalid token
            tokens.push_back(Token(tt.value(), std::string(yytext), row_position, col_position));
            col_position += yyleng;
            auto val = tt.value();
            if (val == LEX_TERM_NEWLINE || val == LEX_NOP_COMMENT) {
                ++row_position;
                col_position = 1;
            }
        } else {
            return std::make_unique<TokenizeError>(row_position, col_position, yytext);
        }
        new_token = yylex();
    }
#ifdef DEBUG
    std::cerr << "Done reading tokens" << std::endl;
#endif
    return TokenString(tokens);
}

std::variant<TokenString, std::unique_ptr<Error>>
TokenString::try_tokenize_file(std::string const &path) {
    FILE *file = fopen(path.c_str(), "r");
    if (!file) {
        return std::make_unique<IOError>(path);
    }
    auto a = try_tokenize(file);
    fclose(file);
    return a;
}

std::variant<TokenString, std::unique_ptr<Error>> TokenString::try_tokenize_stdin() {
    return try_tokenize(nullptr);
}

void TokenString::print_debug() const {
    for (auto token : this->tokens) {
        token.print_debug();
    }
}

void TokenString::clean() {
    std::vector<Token> cleaned_tokens;
    for (auto token : this->tokens) {
        switch (token.get_token_type()) {
        case LEX_NOP_COMMENT:
            cleaned_tokens.push_back(
                Token(LEX_TERM_NEWLINE, "\n", token.get_row(), token.get_col()));
        case LEX_NOP_WHITESPACE:
            continue;

        default:
            cleaned_tokens.push_back(token);
        }
    }
    this->tokens = cleaned_tokens;
}
