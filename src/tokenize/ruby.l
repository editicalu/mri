/* Lex file, used to read the contents of the MiniRuby file */

%{
#include "TokenType.h"
  /* Keep track of current position of lex for error messages, i.e. 
     the position just *after* the last token read */
  int line_nr = 1;
  int col_nr = 1; 

%}
%%

[+]                  { return LEX_BINOP_PLUS; }
[-]                  { return LEX_BINOP_MINUS; }
[*]                  { return LEX_BINOP_TIMES; }
[/]                  { return LEX_BINOP_DIV; }
[<][=]                { return LEX_BINOP_LE; }
[<]                  { return LEX_BINOP_LT; }
[>][=]               { return LEX_BINOP_GE; }
[>]                  { return LEX_BINOP_GT; }
[=]{2}               { return LEX_BINOP_ISEQ; }
[!][=]               { return LEX_BINOP_ISNOTEQ; }
[&]{2}               { return LEX_BINOP_AND; }
[|]{2}               { return LEX_BINOP_OR; }
[!]                  { return LEX_CHAR_NOT; }

[=]                  { return LEX_ASSIGN_EQ; }
[+][=]               { return LEX_ASSIGN_PLUSEQ; }
[-][=]               { return LEX_ASSIGN_MINUSEQ; }
[*][=]               { return LEX_ASSIGN_TIMESEQ; }
[/][=]               { return LEX_ASSIGN_DIVEQ; }
[&][=]               { return LEX_ASSIGN_ANDEQ; }
[|][=]               { return LEX_ASSIGN_OREQ; }

\n                   { return LEX_TERM_NEWLINE; }
;                    { return LEX_TERM_SEMICOLON; }

undef                { return LEX_KEYWORD_UNDEF; }
def                  { return LEX_KEYWORD_DEF; }
do                   { return LEX_KEYWORD_DO; }
end                  { return LEX_KEYWORD_END; }
then                 { return LEX_KEYWORD_THEN; }
if                   { return LEX_KEYWORD_IF; }
elsif                { return LEX_KEYWORD_ELSIF; }
else                 { return LEX_KEYWORD_ELSE; }
unless               { return LEX_KEYWORD_UNLESS; }
until                { return LEX_KEYWORD_UNTIL; }
while                { return LEX_KEYWORD_WHILE; }
case                 { return LEX_KEYWORD_CASE; }
when                 { return LEX_KEYWORD_WHEN; }
true                 { return LEX_KEYWORD_TRUE; }
false                { return LEX_KEYWORD_FALSE; }
return               { return LEX_KEYWORD_RETURN; }

[0-9]+                   { return LEX_SYM_INTEGER; }
[0-9]*\.[0-9]+           { return LEX_SYM_FLOAT; }
[a-zA-Z_][a-zA-Z0-9_]*  { return LEX_SYM_IDENTIFIER; }


[#].+\n              { return LEX_NOP_COMMENT; }
[ \t\n]              { return LEX_NOP_WHITESPACE; }
,                    { return LEX_CHAR_COMMA; }

\(                   { return LEX_PAIRS_LPAR; }
\)                   { return LEX_PAIRS_RPAR; }
\{                   { return LEX_PAIRS_LBRA; }
\}                   { return LEX_PAIRS_RBRA; }

.                    { return -1; }

%%
int yywrap() {
  return 1;
}
