#ifndef LEXTRANSLATOR_HPP
#define LEXTRANSLATOR_HPP

#include "TokenType.h"

#include <optional>

std::optional<TokenType> lex_index_to_tokentype(int lex_value);

#endif
