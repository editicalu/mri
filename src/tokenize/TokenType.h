//! This file contains all the possible tokens that we can find in our MiniRuby program. These
//! values will be used by the Lex parser.
//!
//! This file differs a lot from the example given by the professor, as we take advantage of the
//! possibilities given by the C-enums.

#ifndef TOKENTYPE_H
#define TOKENTYPE_H

enum TokenType {
    // Binary operators
    LEX_BINOP_PLUS = 1,
    LEX_BINOP_MINUS = 2,
    LEX_BINOP_TIMES = 3,
    LEX_BINOP_DIV = 4,
    LEX_BINOP_ISEQ = 5,
    LEX_BINOP_ISNOTEQ = 6,
    LEX_BINOP_GT = 7,
    LEX_BINOP_GE = 8,
    LEX_BINOP_LT = 9,
    LEX_BINOP_LE = 10,
    LEX_BINOP_AND = 11,
    LEX_BINOP_OR = 12,

    // Assignment operators
    LEX_ASSIGN_EQ = 100,
    LEX_ASSIGN_PLUSEQ = 101,
    LEX_ASSIGN_MINUSEQ = 102,
    LEX_ASSIGN_TIMESEQ = 103,
    LEX_ASSIGN_DIVEQ = 104,
    LEX_ASSIGN_ANDEQ = 105,
    LEX_ASSIGN_OREQ = 106,

    // Terminator
    LEX_TERM_NEWLINE = 200,
    LEX_TERM_SEMICOLON = 201,

    // Symbols
    LEX_SYM_IDENTIFIER = 300,
    LEX_SYM_INTEGER = 301,
    LEX_SYM_FLOAT = 302,

    // Keywords
    LEX_KEYWORD_UNDEF = 400,
    LEX_KEYWORD_DEF = 401,
    LEX_KEYWORD_END = 402,
    LEX_KEYWORD_THEN = 403,
    LEX_KEYWORD_IF = 404,
    LEX_KEYWORD_ELSIF = 405,
    LEX_KEYWORD_ELSE = 406,
    LEX_KEYWORD_UNLESS = 407,
    LEX_KEYWORD_WHILE = 408,
    LEX_KEYWORD_CASE = 409,
    LEX_KEYWORD_WHEN = 410,
    LEX_KEYWORD_DO = 411,
    LEX_KEYWORD_TRUE = 412,
    LEX_KEYWORD_FALSE = 413,
    LEX_KEYWORD_RETURN = 414,
    LEX_KEYWORD_UNTIL = 415,

    // Literal characters
    LEX_CHAR_COMMA = 501,
    LEX_CHAR_NOT = 502,

    // Pairs (mostly the different types of braces)
    LEX_PAIRS_LPAR = 600,
    LEX_PAIRS_RPAR = 601,
    LEX_PAIRS_LBRA = 602,
    LEX_PAIRS_RBRA = 603,

    // No Operations
    LEX_NOP_WHITESPACE = 700,
    LEX_NOP_COMMENT = 701,
};

typedef enum TokenType TokenType;

#endif
