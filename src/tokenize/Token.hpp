#ifndef TOKEN_HPP
#define TOKEN_HPP

#include "TokenType.h"
#include <string>

class Token {
  private:
    TokenType token_id;
    std::string raw_contents;
    size_t row;
    size_t col;

  public:
    Token(TokenType token_id, std::string raw_contents, size_t row, size_t col);

    /// Returns the row of the current Token.
    [[nodiscard]] size_t get_row() const;
    [[nodiscard]] size_t get_col() const;

    [[nodiscard]] TokenType get_token_type() const;

    /// Prints debug information to the stderr.
    void print_debug() const;

    [[nodiscard]] std::string const &get_raw() const;
};

/// Returns the name of the TokenType. This should be used mainly for debugging purposes.
std::string get_tokentype_meaning(TokenType tt);

#endif