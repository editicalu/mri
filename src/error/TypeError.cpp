#include "TypeError.hpp"

TypeError::TypeError(std::string operation, DataType d)
    : Error(), operation(operation), d1(d), d2(std::nullopt) {}

TypeError::TypeError(std::string operation, DataType d1, DataType d2)
    : Error(), operation(operation), d1(d1), d2(d2) {}

std::string TypeError::get_name() const { return "TypeError"; }

std::string TypeError::get_explanation() const {
    if (!this->d2.has_value()) {
        return "Cannot apply operation " + this->operation + "' on type '" +
               datatype_to_string(this->d1) + "'.\n";
    }

    return "Cannot apply operation '" + this->operation + "' on types '" +
           datatype_to_string(this->d1) + "' and '" + datatype_to_string(this->d2.value()) + "'.\n";
}
