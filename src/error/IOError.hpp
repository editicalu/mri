#ifndef IOERROR_HPP
#define IOERROR_HPP

#include "Error.hpp"

/// Error that happened because the input file could not be read.
class IOError : public Error {
  private:
    /// File that we could not read out.
    std::string filename;

  public:
    explicit IOError(std::string filename);

    // Inherited methods
    // See Error.hpp for documentation on these.
    [[nodiscard]] std::string get_name() const override;
    [[nodiscard]] std::string get_explanation() const override;
};

#endif
