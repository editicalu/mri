#ifndef UNREACHABLECODEERROR_HPP
#define UNREACHABLECODEERROR_HPP

#include "Error.hpp"

/// This error is meant for cases which the developer thaught would never occur. These are mostly
/// used on switch-cases where a default makes no sense.
class UnreachableCodeError : public Error {
    /// A unique identifier that is used to indicate where the unreachable code was reached.
    std::string uid;

  public:
    explicit UnreachableCodeError(std::string uid);

    // Inherited methods
    // See Error.hpp for documentation on these.
    [[nodiscard]] std::string get_name() const override;

    [[nodiscard]] std::string get_explanation() const override;
};

#endif
