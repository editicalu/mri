#include "SyntaxError.hpp"
#include <string>

SyntaxError::SyntaxError(char const *yacc_error_message, Token const *token)
    : yacc_error_message(yacc_error_message), token(token) {}

std::string SyntaxError::get_name() const { return std::string("SyntaxError"); }

std::string SyntaxError::get_explanation() const {
    return std::string("Could not parse the tokens in your code. Check your code for errors.\n") +
           std::string("Error with token ") + get_tokentype_meaning(token->get_token_type()) +
           std::string(" aka '") + token->get_raw() + std::string("'") + std::string(" at row ") +
           std::to_string(token->get_row()) + " col " + std::to_string(token->get_col()) +
           std::string("\nYacc: ") + this->yacc_error_message;
}
