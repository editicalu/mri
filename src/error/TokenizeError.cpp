#include "TokenizeError.hpp"

TokenizeError::TokenizeError(std::pair<size_t, size_t> location, std::string unrecognized_token)
    : location(location), unrecognized_token(unrecognized_token) {}

TokenizeError::TokenizeError(size_t row, size_t col, std::string unrecognized_token)
    : location(std::pair(row, col)), unrecognized_token(unrecognized_token) {}

std::string TokenizeError::get_name() const { return std::string("TokenizeError"); }

std::string TokenizeError::get_explanation() const {
    return std::string("Undefined token at row ") + std::to_string(this->location.first) + " col " +
           std::to_string(this->location.second) + ": '" + this->unrecognized_token + "'";
}
