#ifndef SYMBOLERROR_HPP
#define SYMBOLERROR_HPP

#include "Error.hpp"

/// User used a symbol (variable) that was not defined.
class SymbolError : public Error {
    /// The symbol the user called.
    std::string variable_name;

  public:
    explicit SymbolError(std::string variable_name);

    // Inherited methods
    // See Error.hpp for documentation on these.
    [[nodiscard]] std::string get_name() const override;
    [[nodiscard]] std::string get_explanation() const override;
};

#endif
