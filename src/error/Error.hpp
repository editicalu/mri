#pragma once
#ifndef ERROR_HPP
#define ERROR_HPP

#include <iostream>
#include <string>

/// Abstract class that represents any error that could occur in the program. All things that could
/// go wrong, should be derived from this.
///
/// Because of this, error handling is as easy as calling explain_on_stderr() in main, which gives
/// compact, maintainable code.
class Error {
  public:
    /// Returns a verbose name of the Error. This is most likely just the classname.
    [[nodiscard]] virtual std::string get_name() const = 0;

    /// Explains the error, why it happened and how the user could solve it.
    [[nodiscard]] virtual std::string get_explanation() const = 0;

    /// Prints both get_name() and get_explanation() to the std:cerr. Useful when sowing the error
    /// to the user.
    void explain_on_stderr() const;
};

#endif