#ifndef FUNCTIONCALLERROR_HPP
#define FUNCTIONCALLERROR_HPP

#include "Error.hpp"

/// There was a mismatching amount of arguments when calling a specific function.
class FunctionCallError : public Error {
    /// The amount of parameters that were given by the user.
    size_t given;

    /// The amount of parameters required by the particular function.
    size_t required;

    /// The name of the function that was called.
    std::string function_name;

  public:
    FunctionCallError(size_t arguments_given, size_t arguments_required, std::string function_name);

    // Inherited methods
    // See Error.hpp for documentation on these.
    [[nodiscard]] std::string get_name() const override;
    [[nodiscard]] std::string get_explanation() const override;
};

#endif
