#include "InvalidNumberError.hpp"

InvalidNumberError::InvalidNumberError(std::string number) : number(number) {}

std::string InvalidNumberError::get_name() const { return std::string("InvalidNumberError"); }

std::string InvalidNumberError::get_explanation() const {
    return std::string("Could not convert the number ") + this->number + std::string("to a number");
}
