#include "SymbolError.hpp"

SymbolError::SymbolError(std::string variable_name) : variable_name(variable_name) {}

std::string SymbolError::get_name() const { return std::string("SymbolError"); }

std::string SymbolError::get_explanation() const {
    return "Use of undefined symbol '" + this->variable_name + "'\n";
}
