#include "IOError.hpp"
#include <iostream>

IOError::IOError(std::string filename) : filename(filename) {}

std::string IOError::get_name() const { return std::string("IOError"); }

std::string IOError::get_explanation() const {
    return "Could not open file '" + this->filename +
           "' for reading. Make sure the file exists and can be read by the current user.";
}
