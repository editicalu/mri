#include "DivideByZeroError.hpp"

DivideByZeroError::DivideByZeroError() {}

std::string DivideByZeroError::get_explanation() const { return "Cannot divide by zero1!\n"; }

std::string DivideByZeroError::get_name() const { return "DivideByZeroError"; }
