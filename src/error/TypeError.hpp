#ifndef TYPEERROR_HPP
#define TYPEERROR_HPP

#include <optional>
#include <string>

#include "../data/DataType.hpp"
#include "Error.hpp"

/// A Value was used on a place where it was not compatible. e.g. 1+false or `if "foo"`.
class TypeError : public Error {
  private:
    std::string operation;

    DataType d1;
    std::optional<DataType> d2;

  public:
    TypeError(std::string operation, DataType d);

    TypeError(std::string operation, DataType d1, DataType d2);

    // Inherited methods
    // See Error.hpp for documentation on these.
    [[nodiscard]] std::string get_name() const override;
    [[nodiscard]] std::string get_explanation() const override;
};

#endif
