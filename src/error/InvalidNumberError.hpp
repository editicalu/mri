#ifndef INVALIDNUMBERERROR_HPP
#define INVALIDNUMBERERROR_HPP

#include "Error.hpp"

/// The given number could not be parsed. Perhaps it is invalid or out of range.
class InvalidNumberError : public Error {
  private:
    /// The number as it was read from the file.
    std::string number;

  public:
    explicit InvalidNumberError(std::string number);

    // Inherited methods
    // See Error.hpp for documentation on these.
    [[nodiscard]] std::string get_name() const override;
    [[nodiscard]] std::string get_explanation() const override;
};

#endif
