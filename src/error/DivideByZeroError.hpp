#ifndef DIVISIONBYZEROERROR_HPP
#define DIVISIONBYZEROERROR_HPP

#include <string>

#include "Error.hpp"

/// Error given by trying to divide by zero.
class DivideByZeroError : public Error {
  public:
    DivideByZeroError();

    // Inherited methods
    // See Error.hpp for documentation on these.
    [[nodiscard]] std::string get_name() const override;
    [[nodiscard]] std::string get_explanation() const override;
};

#endif
