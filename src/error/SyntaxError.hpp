#ifndef SYNTAXERROR_HPP
#define SYNTAXERROR_HPP

#include "../tokenize/Token.hpp"
#include "Error.hpp"

/// A SyntaxError represents the error that is caused when parsing fails, but tokenizing succeeds.
class SyntaxError : public Error {
    /// Position in the array of tokens where this program failed parsing.
    std::string yacc_error_message;
    /// The token on which the error occurs.
    Token const *token;

  public:
    SyntaxError(char const *yacc_error_message, Token const *token);

    // Inherited methods
    // See Error.hpp for documentation on these.
    [[nodiscard]] std::string get_name() const override;
    [[nodiscard]] std::string get_explanation() const override;
};

#endif
