#include "Error.hpp"
#include <iostream>

void Error::explain_on_stderr() const {
    std::cerr << this->get_name() << ": " << this->get_explanation() << std::endl;
}
