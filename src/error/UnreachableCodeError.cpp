#include "UnreachableCodeError.hpp"

UnreachableCodeError::UnreachableCodeError(std::string uid) : uid(uid) {}

std::string UnreachableCodeError::get_name() const { return std::string("UnreachableCodeError"); }

std::string UnreachableCodeError::get_explanation() const {
    return "Normally, you shouldn't be able to reach this code. You're currently at " + this->uid +
           "\n";
}
