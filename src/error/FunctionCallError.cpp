#include "FunctionCallError.hpp"

FunctionCallError::FunctionCallError(
    size_t arguments_given,
    size_t arguments_required,
    std::string function_name)
    : given(arguments_given), required(arguments_required), function_name(function_name) {}

std::string FunctionCallError::get_name() const { return std::string("FunctionCallError"); }

std::string FunctionCallError::get_explanation() const {
    return "function " + this->function_name + " requires " + std::to_string(this->required) +
           " arguments, " + std::to_string(this->given) + " were given";
}
