#ifndef TOKENIZEERROR_HPP
#define TOKENIZEERROR_HPP

#include "Error.hpp"
#include <string>
#include <utility>

/// There was a token that could not be parsed.
class TokenizeError : public Error {
    /// Location in the file, denoted as row, column.
    std::pair<size_t, size_t> location;
    /// The string that could not be recognized as a token
    std::string unrecognized_token;

  public:
    TokenizeError(std::pair<size_t, size_t> location, std::string unrecognized_token);

    TokenizeError(size_t row, size_t col, std::string unrecognized_token);

    // Inherited methods
    // See Error.hpp for documentation on these.
    [[nodiscard]] std::string get_name() const override;
    [[nodiscard]] std::string get_explanation() const override;
};

#endif