# How this works

The Python script runs will expect the same structure in every `./src`-subfolder:
- `folderName.rb` with the script to run
- `folderName.out` with expected stdout
- `folderName.err` with expected stderr

How the tester works is pretty simple:

1. It will create a test for every subfolder of the `./src`-directory.
1. Every test Ruby script will be given as parameter to the compiled interpreter.
1. It will compare the expected output with the actual output.

It is made to exit with a failure return code so that it can be used in CI applications.


