#!/bin/python3
from pathlib import Path
import subprocess
import sys
import difflib

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

bin_location = (Path(__file__).parent / '..' / 'src' / 'mri').resolve()
test_location = Path(__file__).parent / 'src' 

def one_test(name):
    """
        Tries to run the test called name
        Returns whether the test succeeded.
    """

    directory = Path(__file__).parent / 'src' / name
    proc_path = (directory / (name + '.rb')).resolve()
    stdout_path = (directory / (name + '.stdout')).resolve()
    stderr_path = (directory / (name + '.stderr')).resolve()

    sub = subprocess.run([bin_location, proc_path], capture_output=True)
    diffs = ""

    stdout = sub.stdout.decode('utf-8')
    expected_stdout = str(stdout_path.read_text())
    out_ok = stdout == expected_stdout
    if not out_ok:
        diffs += "".join(list(difflib.context_diff(stdout, expected_stdout, fromfile='stdout_run', tofile='stdout_expected')))

    stderr = sub.stderr.decode('utf-8')
    expected_stderr = str(stderr_path.read_text())
    err_ok = stderr == expected_stderr
    if not err_ok:
        diffs += "".join(list(difflib.context_diff(stderr, expected_stderr, fromfile='stderr_run', tofile='stderr_expected')))

    ok = out_ok and err_ok 
    return ok, diffs

fail = False
NAME_WIDTH = 50
for test in [x for x in test_location.resolve().iterdir() if x.is_dir()]:
    (now_success, diff) = one_test(test.name)
    if now_success:
        print(bcolors.OKGREEN + "SUCCESS" + bcolors.ENDC, end=" ")
    else:
        print(bcolors.FAIL + "FAIL   " + bcolors.ENDC, end=" ")

    print(test.name)
    if len(sys.argv) > 1 and sys.argv[1] == test.name:
        print(diff)

    fail = fail or not now_success

if fail:
    sys.exit(1)

