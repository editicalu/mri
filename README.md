# MRI: MiniRuby Interpreter

Interpreter for a very small subset of Ruby.

This project was built for the course [Compilers](https://uhintra03.uhasselt.be/studiegidswww/opleidingsonderdeel.aspx?a=2019&i=2167) at [Hasselt University](https://uhasselt.be).
# Building

To build the program, you need the following dependencies:
- Bison
- CMake
- Flex
- gcc and g++ (or different but unsupported CXX compilers)
- Make

The CI ensures that this project should be able to compile on Ubuntu 20.04 LTS and Ubuntu 19.10.

Use the bash scripts `debugbuild.sh` or `releasebuild.sh` to compile the program. Debug mode adds a lot of printing to the command line.

# Running

The application takes its input from stdin if no input files were given. Use the `-h`/`--help` flag for all options.

# Project Structure

The project consists of the following components:

- data: structures to handle the storage and usage of variables and values. Support for integers, floats, strings and functions is included.
- error: module with all the possible errors of our program. This gives our program a uniform interface for error handling, which makes it easier to handle them throughout the program.
- helper_fn: several small helper functions that do not have a big impact on the project.
- parsing: build the parsing tree from the tokens. Heavily depends on Yacc/Bison.
- tokenize: interaction with (f)lex and everything to tokenize the project.
- tree: all available components of the program. This includes the statements and expressions and how to use them.

# Testing

Inside the `testing` folder, you can find a small script that tests the binary against some test cases. It is important to compile in release mode in order for the tests to work.